package com.mobilesoft.italyformovies.activity

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.mobilesoft.data.model.Filter
import com.mobilesoft.italyformovies.R
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        iv_film_game.setOnClickListener {
            buttoAnimation(it, 1.0f, 0.8f) {
                startMainActivity(Filter.FILM_AND_GAMES)

            }.start()
        }

        iv_location_incentive.setOnClickListener {
            buttoAnimation(it, 1.0f, 0.8f) {
                startMainActivity(Filter.LOCATION)

            }.start()
        }
        cl_icon_group.alpha = 0.0f
        iv_background_blur.alpha = 0.0f


        val rootSet = AnimatorSet()
        val fadeIconGroup = fadeAnimation(cl_icon_group, 0.0f, 1.0f)
        val showBackgroundBlur = fadeAnimation(iv_background_blur, 0.0f, 0.4f)
        val fadeBackground = fadeAnimation(iv_background, 0.4f, 0.0f)

        rootSet.playTogether(
            fadeIconGroup,
            showBackgroundBlur,
            fadeBackground

        )
        rootSet.start()
    }

    private fun startMainActivity(filter: Filter) {
        val intent = Intent(this, MainActivity::class.java)
        val mBundle = Bundle()
        mBundle.putString(ARG_POI, filter.id)
        intent.putExtras(mBundle)
        startActivity(Intent(intent))
        finish()
    }

    fun fadeAnimation(view: View, startOpacity: Float, endOpacity: Float): ObjectAnimator {

        val fadeAnimator = ObjectAnimator.ofFloat(view, "alpha", startOpacity, endOpacity)
        fadeAnimator?.apply {
            duration = 3000
        }
        return fadeAnimator
    }

    fun buttoAnimation(view: View, startSize: Float, endSize: Float, onAnimationEnd: () -> Unit): AnimatorSet {
        val scaleAnimSet = AnimatorSet()

        val scaleXAnimator = ObjectAnimator.ofFloat(view, "scaleX", startSize, endSize)
        scaleXAnimator?.apply {
            duration = 30
        }
        val scaleYAnimator = ObjectAnimator.ofFloat(view, "scaleY", startSize, endSize)
        scaleYAnimator?.apply {
            duration = 30
        }
        scaleAnimSet.playTogether(scaleXAnimator, scaleYAnimator)
        scaleAnimSet.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
            }

            override fun onAnimationEnd(animation: Animator?) {
                onAnimationEnd.invoke()

            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationStart(animation: Animator?) {

            }

        })
        return scaleAnimSet
    }

    companion object {
        const val ARG_POI = "poiArg"
    }
}
