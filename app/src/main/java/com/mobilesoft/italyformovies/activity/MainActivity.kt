package com.mobilesoft.italyformovies.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.View
import androidx.annotation.DrawableRes
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.fragment.*
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener.Command
import com.mobilesoft.italyformovies.adapter.MenuAdapter
import com.mobilesoft.italyformovies.ui.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mobilesoft.data.model.Filter
import com.mobilesoft.italyformovies.activity.SplashActivity.Companion.ARG_POI
import com.mobilesoft.italyformovies.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.fragment_map.*


class MainActivity : SuperAppCompactActivity(),
    OnFragmentInteractionListener {

    private var mDrawerToggle: ActionBarDrawerToggle? = null
    private val menuItems: List<MenuItem> by lazy { populateMenu() }
    private var currenFragment: String? = null
    private var _menu: Menu? = null
    private var mToolBarNavigationListenerIsRegistered = false
    lateinit var mViewModel: MainViewModel

    override fun onCommand(command: Command) {

        when (command) {

            Command.ShowMap -> {
                navigateTo(MapFragment(), tagTransaction = BACK_STACK_ROOT_TAG)
            }
            Command.ShowNewsDetail -> {
                navigateTo(NewsDetailFragment())
            }
            Command.ShowLoader -> {
                showPopupProgressDialog(true)
            }
            Command.ShowMovieDetail -> {
                navigateTo(MovieDetailFragment())
            }
            Command.ShowLocationDetail -> {

                navigateTo(LocationDetailFragment())
            }
            Command.ShowGameDetail -> {

                navigateTo(GameDetailFragment())
            }
            Command.ShowIncentiveDetail -> {
                navigateTo(IncentiveDetailFragment())
            }

            Command.ShowIncentives -> {
                navigateTo(IncentiveListFragment())
            }

            Command.HideLoader -> {
                showPopupProgressDialog(false)
            }
            is Command.TrackFragment -> {
                currenFragment = command.fragmentTag
                setViewFromVisibleFragment()
            }
        }
    }

    private fun setViewFromVisibleFragment() {
        when (currenFragment) {
            MapFragment.TAG -> {
                setNavigationDrawer()
                setHeaderMenu(R.drawable.ic_instagram)
            }
            MovieListFragment.TAG,
            GameListFragment.TAG,
            IncentiveListFragment.TAG,
            NewsListFragment.TAG,
            LocationListFragment.TAG -> {
                setNavigationDrawer()
                setHeaderMenu(R.drawable.ic_search)
            }
            MovieDetailFragment.TAG,
            LocationDetailFragment.TAG,
            GameDetailFragment.TAG,
            IncentiveDetailFragment.TAG -> {
                setHomeNavigation()
                setHeaderMenu(R.drawable.ic_instagram)
            }
            SearchLocationFragment.TAG,
            IncentiveSearchFragment.TAG,
            MovieSearchFragment.TAG,
            SearchGameFragment.TAG -> {
                setHomeNavigation()
                setHeaderMenu(null)
            }
            else -> setHomeNavigation()
        }
    }

    private fun setHeaderMenu(@DrawableRes actionIcon: Int?) {
        if (actionIcon == null) {
            _menu?.getItem(0)?.isVisible = false
        } else {
            _menu?.getItem(0)?.icon = ContextCompat.getDrawable(this, actionIcon)
            _menu?.getItem(0)?.isVisible = true
        }
    }


    private fun setNavigationDrawer() {
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        mDrawerToggle?.isDrawerIndicatorEnabled = true
        mDrawerToggle?.toolbarNavigationClickListener = null
        mToolBarNavigationListenerIsRegistered = false
    }

    private fun setHomeNavigation() {
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        mDrawerToggle?.isDrawerIndicatorEnabled = false
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        if (!mToolBarNavigationListenerIsRegistered) {
            mDrawerToggle?.setToolbarNavigationClickListener {
                onBackPressed()
            }

            mToolBarNavigationListenerIsRegistered = true
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        mViewModel.poiFilter = intent.getStringExtra(ARG_POI)
       if( mViewModel.poiFilter.equals("0"))
       {
           ib_incentive?.visibility =  View.GONE
       }

        mViewModel.getError().observe(this, Observer { message ->
            showPopupProgressDialog(false)
            showErrorDialog(message)
        })
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayShowTitleEnabled(false)

        lv_navigation_menu.adapter = MenuAdapter(this, menuItems)

        lv_navigation_menu.setOnItemClickListener { parent, view, position, id ->

            when (menuItems[position].tag) {
                LocationListFragment.TAG -> {
                    navigateTo(LocationListFragment())
                }
                MapFragment.TAG -> {

                    navigateTo(MapFragment(), tagTransaction = BACK_STACK_ROOT_TAG)
                }
                MovieListFragment.TAG -> {

                    navigateTo(MovieListFragment())
                }
                IncentiveListFragment.TAG -> navigateTo(IncentiveListFragment())
                NewsListFragment.TAG -> {
                    navigateTo(NewsListFragment())
                }
                GameListFragment.TAG -> {

                    navigateTo(GameListFragment())
                }
            }

            if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                drawer_layout.closeDrawer(GravityCompat.START)
            }
        }

        initToggle()
        setViewFromVisibleFragment()
        //    nav_view.setNavigationItemSelectedListener(this)
        navigateTo(MapFragment(), tagTransaction = BACK_STACK_ROOT_TAG)
    }

    private fun initToggle() {

        mDrawerToggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        if (mDrawerToggle != null) {
            drawer_layout.addDrawerListener(mDrawerToggle!!)
            mDrawerToggle?.syncState()
        }
    }


    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {

            if (currenFragment == MapFragment.TAG) {
                showExitDialog()
            } else {
                supportFragmentManager.popBackStack()
                setViewFromVisibleFragment()
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        _menu = menu
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)

        return true
    }

    override fun onOptionsItemSelected(item: android.view.MenuItem?): Boolean {
        if (item?.itemId == R.id.main_action) {

                    when (currenFragment) {
                        LocationListFragment.TAG -> {
                            navigateTo(SearchLocationFragment())
                        }
                        MovieListFragment.TAG -> {
                            navigateTo(SearchMovieFragment())
                        }
                        GameListFragment.TAG -> {
                            navigateTo(SearchGameFragment())
                        }
                        MapFragment.TAG -> {
                           val intent = Intent("android.media.action.IMAGE_CAPTURE")
                            startActivity(intent)
                        }
                    }
                }

        return true
    }


    fun navigateTo(fragment: Fragment, addToBackStack: Boolean = true, tagTransaction: String? = null) {

        currenFragment = fragment::class.java.simpleName
        Log.d(TAG, "Navigate to fragment is $currenFragment")


        if (currenFragment == MapFragment.TAG && supportFragmentManager.backStackEntryCount > 0) {
            Log.d(TAG, "Cleaning all the back stack ")
            supportFragmentManager.popBackStack(BACK_STACK_ROOT_TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }

        val ft = supportFragmentManager.beginTransaction().apply {
            replace(R.id.container, fragment)
            if (addToBackStack) {
                addToBackStack(tagTransaction)
            }
        }
        ft.commit()
    }

    private fun populateMenu(): List<MenuItem> {
        val menuItems = ArrayList<MenuItem>()
        menuItems.add(MenuItem(R.drawable.ic_menu_map, R.string.map, MapFragment.TAG))
        menuItems.add(MenuItem(R.drawable.ic_menu_location, R.string.location, LocationListFragment.TAG))
        menuItems.add(MenuItem(R.drawable.ic_menu_film, R.string.film, MovieListFragment.TAG))
        menuItems.add(MenuItem(R.drawable.ic_menu_game, R.string.game, GameListFragment.TAG))
        menuItems.add(MenuItem(R.drawable.ic_menu_itineraries, R.string.itineraries, ""))
        menuItems.add(MenuItem(R.drawable.ic_menu_incentives, R.string.incentives, IncentiveListFragment.TAG))
        menuItems.add(MenuItem(R.drawable.ic_menu_instagram, R.string.gallery, ""))
        menuItems.add(MenuItem(R.drawable.menu_news, R.string.news, NewsListFragment.TAG))
        menuItems.add(MenuItem(R.drawable.menu_info, R.string.info, ""))
        menuItems.add(MenuItem(R.drawable.menu_tutorial, R.string.tutorial, ""))
        menuItems.add(MenuItem(R.drawable.menu_settings, R.string.settings, ""))

        return menuItems
    }

    fun showExitDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(R.string.exit_dialog_description)
        builder.setPositiveButton(R.string.confirm) { dialog, which ->
            dialog.cancel()
            finish()
        }
        builder.setNegativeButton(R.string.cancel
        ) { dialog, which -> dialog.cancel() }
        builder.show()
    }

    companion object {
        val TAG: String = MainActivity::class.java.simpleName

        const val BACK_STACK_ROOT_TAG: String = "root_fragment"
    }
}
