package com.mobilesoft.italyformovies.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;
import com.mobilesoft.data.model.Image;
import com.mobilesoft.italyformovies.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImagePagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    List<Image> mImages;

    public ImagePagerAdapter(Context context, List<Image> images) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mImages = images;
    }

    @Override
    public int getCount() {
        return mImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.image_pager_view, container, false);

        Image image = mImages.get(position);

        ImageView imageBackground = itemView.findViewById(R.id.iv_background);
        TextView textImageName = itemView.findViewById(R.id.tv_image_name);
        textImageName.setText(image.getName());
        Picasso.with(mContext).load(image.getUrlImg()).placeholder(R.drawable.image_placeholder).into(imageBackground);


        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ConstraintLayout) object);
    }
}