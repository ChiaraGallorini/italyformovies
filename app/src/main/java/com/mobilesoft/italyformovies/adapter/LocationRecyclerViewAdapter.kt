package com.mobilesoft.italyformovies.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mobilesoft.data.model.Location
import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.ui.Region
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_location.view.*

class LocationRecyclerViewAdapter(
    private val context: Context,
    private val mListener: (Location) -> Unit
) : RecyclerView.Adapter<LocationRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener
    private val mLocations: MutableList<Location> = ArrayList()

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Location
            mListener.invoke(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_location, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mLocations[position]

        with(holder.mView) {
            tag = item

            setOnClickListener(mOnClickListener)
        }
        with(holder) {
            textLocation.text = item.title
            textRegione.text = Region(context, item.region).name
            Picasso.with(context).load(item.image).placeholder(R.drawable.image_placeholder)
                .into(holder.imageBackground)
        }
    }

    fun setItems(items: List<Location>) {
        mLocations.clear()
        mLocations.addAll(items)
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int = mLocations.size
    fun addItems(items: List<Location>) {
        mLocations.addAll(items)
        notifyDataSetChanged()
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val textLocation: TextView = mView.tv_location
        val textRegione: TextView = mView.tv_region
        val imageBackground: ImageView = mView.iv_locations
    }
}
