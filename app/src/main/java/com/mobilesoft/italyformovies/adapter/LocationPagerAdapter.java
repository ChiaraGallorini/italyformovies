package com.mobilesoft.italyformovies.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import com.mobilesoft.data.model.Location;
import com.mobilesoft.italyformovies.fragment.LocationFragment;

import java.util.List;

public class LocationPagerAdapter extends FragmentPagerAdapter {

    private List<Location> mLocations;


    public LocationPagerAdapter(FragmentManager fm, List<Location> list) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mLocations = list;
    }

    @Override
    public Fragment getItem(int position) {
        Location location = mLocations.get(position);

        return LocationFragment.newInstance(null,
                null, null);
    }

    @Override
    public int getCount() {
        return mLocations.isEmpty() ? 0 : mLocations.size();
    }

    @Override
    public float getPageWidth(int position) {
        return 0.93f;
    }
}