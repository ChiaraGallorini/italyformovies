package com.mobilesoft.italyformovies.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.mobilesoft.data.model.Game
import com.mobilesoft.italyformovies.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_cover.view.*
import java.util.*


class GameRecyclerViewAdapter(
    private val context: Context?,
    private val mListener: (Game) -> Unit


) : RecyclerView.Adapter<GameRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener
    private val mValues: MutableList<Game> = ArrayList()


    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Game
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener.invoke(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_cover, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val game = mValues[position]

        with(holder.mView) {
            tag = game

            setOnClickListener(mOnClickListener)
        }
       Picasso.with(context).load(game.cover).placeholder(R.drawable.image_placeholder).into(holder.imageLocation)

    }


    fun addItems(items: List<Game>) {
        mValues.addAll(items)
        notifyDataSetChanged()

    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val imageLocation: ImageView = mView.iv_movie_cover

    }
}





