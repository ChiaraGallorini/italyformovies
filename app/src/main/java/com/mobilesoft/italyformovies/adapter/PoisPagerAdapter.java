package com.mobilesoft.italyformovies.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Group;
import androidx.viewpager.widget.PagerAdapter;
import com.mobilesoft.data.model.*;
import com.mobilesoft.italyformovies.R;
import com.mobilesoft.italyformovies.ui.Region;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PoisPagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    private List<Object> mList = new ArrayList<>();

    public PoisPagerAdapter(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.item_poi_page, container, false);
        ImageView imageCover = itemView.findViewById(R.id.iv_movie_cover);
        ImageView imageBackground = itemView.findViewById(R.id.iv_location_background);
        TextView textTitle = itemView.findViewById(R.id.tv_poi_title);
        TextView textGenre = itemView.findViewById(R.id.tv_poi_genre);
        TextView textYear = itemView.findViewById(R.id.tv_poi_year);
        TextView textLocation = itemView.findViewById(R.id.tv_location);
        TextView textRegion = itemView.findViewById(R.id.tv_region);
        Group groupGames = itemView.findViewById(R.id.group_movie_and_games);
        Group groupLocation = itemView.findViewById(R.id.group_locations);
        groupGames.setVisibility(View.VISIBLE);
        groupLocation.setVisibility(View.GONE);

        if (mList.get(position) instanceof Game) {

            Game game = (Game) mList.get(position);
            Picasso.with(mContext).load(game.getCover()).into(imageCover);
            textTitle.setText(game.getTitle());
            textGenre.setText(game.getGenre());
            textYear.setText(game.getYear());

        } else if (mList.get(position) instanceof Movie) {
            Movie movie = (Movie) mList.get(position);
            Picasso.with(mContext).load(movie.getCover()).into(imageCover);
            textTitle.setText(movie.getTitle());
            textGenre.setText(movie.getGenre());
            textYear.setText(movie.getYear());

        } else if (mList.get(position) instanceof Position) {
            groupGames.setVisibility(View.GONE);
            groupLocation.setVisibility(View.VISIBLE);
            Position pos = (Position) mList.get(position);
            textLocation.setText(pos.getTitle());
            textRegion.setText(new Region(mContext, pos.getRegion()).getName());
            Picasso.with(mContext).load(pos.getImage()).placeholder(R.drawable.image_placeholder).into(imageBackground);
        }
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public void setItems(PoisDetail detail) {
        mList.clear();

        Position loc = new Position(detail.getImage(), detail.getTitle(), detail.getRegion());

         mList.add(0, loc);

        if (detail.getGames() != null && !detail.getGames().isEmpty()) {
            mList.addAll(detail.getGames());
        }
        if (detail.getMovies() != null && !detail.getMovies().isEmpty()) {
            mList.addAll(detail.getMovies());
        }

        this.notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}