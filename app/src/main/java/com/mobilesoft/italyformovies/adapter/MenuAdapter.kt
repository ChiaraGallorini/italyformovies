package com.mobilesoft.italyformovies.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.ui.MenuItem

class MenuAdapter(private val context: Context, private val menutItems: List<MenuItem>) : BaseAdapter() {
    override fun getItem(position: Int): Any {
        return menutItems[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return menutItems.size

    }

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val rowView = inflater.inflate(R.layout.item_navigation_menu, parent, false)
        val textMenu = rowView.findViewById(R.id.tv_menu) as TextView
        val imageMenu = rowView.findViewById(R.id.iv_icon_menu) as ImageView

        val menu = getItem(position) as MenuItem

        if (menu.image == 0) {
            imageMenu.visibility = View.INVISIBLE
        } else {
            imageMenu.setImageResource(menu.image)
        }
        textMenu.text = context.getString(menu.title)

        return rowView
    }
}