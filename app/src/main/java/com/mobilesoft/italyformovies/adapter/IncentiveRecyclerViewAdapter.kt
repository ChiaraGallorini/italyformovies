package com.mobilesoft.italyformovies.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mobilesoft.data.model.Incentive
import com.mobilesoft.italyformovies.R
import kotlinx.android.synthetic.main.item_incentive.view.*


class IncentiveRecyclerViewAdapter(
    private val mListener: (Incentive) -> Unit
) : RecyclerView.Adapter<IncentiveRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener
    private val mIncentives: MutableList<Incentive> = ArrayList()


    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Incentive
            mListener.invoke(item)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_incentive, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val incentive = mIncentives[position]
        holder.mIncentiveName.text = incentive.title

        with(holder.mView) {
            tag = incentive
            setOnClickListener(mOnClickListener)
        }
    }

    fun setItems(items: List<Incentive>) {
        mIncentives.clear()
        mIncentives.addAll(items)
        notifyDataSetChanged()

    }

    override fun getItemCount(): Int = mIncentives.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mIncentiveName: TextView = mView.tv_incentive_name
    }
}
