package com.mobilesoft.italyformovies.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mobilesoft.data.model.News
import com.mobilesoft.italyformovies.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_news.view.*
import java.text.DateFormatSymbols
import java.util.*
import java.text.SimpleDateFormat
import android.graphics.Color
import android.widget.LinearLayout
import androidx.core.content.ContextCompat


class NewsRecyclerViewAdapter(
    private val context: Context,
    private val mListener: (News) -> Unit
) : RecyclerView.Adapter<NewsRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener
    private val mNews: MutableList<News> = ArrayList()
    private val mDateFromSymbols: DateFormatSymbols = DateFormatSymbols()
    private val mRainBow: DateFormatSymbols = DateFormatSymbols()
    private var colors: IntArray = intArrayOf(Color.parseColor("#E5901E"), Color.parseColor("#2072FD"), Color.parseColor("#EF2C36"))
    private var colorPosition = 0


    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as News
            mListener.invoke(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_news, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item = mNews[position]

        if (position % 3 == 0 ) {
            colorPosition = 0
        }

        val color = colors[colorPosition]

        colorPosition++

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
        with(holder) {
            val date = Date(item.date * 1000)
            textMonth.text = mDateFromSymbols.shortMonths[date.month]
            textDay.text = "${date.day}"
            dateBackground.setBackgroundColor(color)
            textNewsTitle.text = item.title
            Picasso.with(context).load(item.image).placeholder(R.drawable.image_placeholder).into(holder.imageNews)
        }
    }

    fun setItems(items: List<News>) {
        mNews.clear()
        mNews.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = mNews.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val textDay: TextView = mView.tv_day
        val textMonth: TextView = mView.tv_month
        val textNewsTitle: TextView = mView.tv_news_title
        val imageNews: ImageView = mView.iv_news
        val dateBackground: LinearLayout = mView.ll_date_background
    }
}
