package com.mobilesoft.italyformovies.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mobilesoft.italyformovies.R;
import com.mobilesoft.italyformovies.ui.Region;
import com.squareup.picasso.Picasso;

public class RegionAdapter extends ArrayAdapter<Region>{

    // Your sent context
    private Context context;
    // Your custom values for the spinner (User)
    private Region[] values;


    public RegionAdapter(Context context, int resourceId,
                       Region[] values) {
        super(context, resourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount(){
        return values.length;
    }

    @Override
    public Region getItem(int position){
        return values[position];
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Region region = values[position];
        View listItem = convertView;
        if(listItem == null) listItem = LayoutInflater.from(context).inflate(R.layout.item_region,parent,false);

        TextView textRegionName = listItem.findViewById(R.id.tv_region_name);
        ImageView imageRegion = listItem.findViewById(R.id.iv_region);

        Picasso.with(context).load(region.getImageResource()).into(imageRegion);
        textRegionName.setText(region.getName());

        return listItem;
    }
}