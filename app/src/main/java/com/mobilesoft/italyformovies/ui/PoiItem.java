package com.mobilesoft.italyformovies.ui;

import com.google.android.gms.maps.model.LatLng;
import com.mobilesoft.data.model.Poi;

public class PoiItem extends GoogleMapItem<Poi> {

    public PoiItem(Poi item) {
        super(item);
        position = new LatLng(item.getLatitude(), item.getLongitude());
    }

    @Override
    public LatLng getPosition() {
        return position;
    }

    @Override
    public String getTitle() {
        return "";
    }

    @Override
    public String getSnippet() {
        return "";
    }

    public Poi getPoi() {
        return item;
    }
}