package com.mobilesoft.italyformovies.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import androidx.core.content.ContextCompat;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.mobilesoft.italyformovies.R;


public class LocationClusterRender extends DefaultClusterRenderer<PoiItem> {

    private static final String TAG = LocationClusterRender.class.getSimpleName();

    private final Context mContext;
    private final IconGenerator mClusterIconGenerator;


    public LocationClusterRender(Context context, final GoogleMap map, ClusterManager<PoiItem> clusterManager) {
        super(context, map, clusterManager);
        mContext = context;
        mClusterIconGenerator = new IconGenerator(context.getApplicationContext());
        UiSettings config = map.getUiSettings();
        config.setMapToolbarEnabled(false);
        config.setZoomControlsEnabled(false);
        clusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<PoiItem>() {
            @Override
            public boolean onClusterItemClick(PoiItem t) {
                return centerCameraOnClusterExpanded(map, t);
            }
        });
    }

    @Override
    protected void onBeforeClusterItemRendered(PoiItem item, MarkerOptions markerOptions) {

        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_location));
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<PoiItem> cluster, MarkerOptions markerOptions) {

            mClusterIconGenerator.setBackground(ContextCompat.getDrawable(mContext, R.drawable.cluster_location));
            mClusterIconGenerator.setTextAppearance(R.style.TextClusterManager);

            final Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
            mClusterIconGenerator.setColor(Color.GREEN);
    }


    @Override
    protected boolean shouldRenderAsCluster(Cluster<PoiItem> cluster) {
        return cluster.getSize() > 1;
    }

    private boolean centerCameraOnClusterExpanded(GoogleMap map, ClusterItem item) {

        LatLngBounds.Builder builder = LatLngBounds.builder();
        builder.include(item.getPosition());
        final LatLngBounds bounds = builder.build();
        try {
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } catch (Exception error) {
            Log.e(TAG, error.getMessage());
        }

        return false;
    }
}
