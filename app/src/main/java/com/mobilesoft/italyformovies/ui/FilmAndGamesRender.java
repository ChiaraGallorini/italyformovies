package com.mobilesoft.italyformovies.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import androidx.annotation.DrawableRes;
import androidx.core.content.ContextCompat;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.mobilesoft.data.model.Filter;
import com.mobilesoft.italyformovies.R;


public class FilmAndGamesRender extends DefaultClusterRenderer<PoiItem> {

    private static final String TAG = FilmAndGamesRender.class.getSimpleName();

    private final Context mContext;
    private final IconGenerator mClusterIconGenerator;
    private Filter mFilter;


    public FilmAndGamesRender(Context context, final GoogleMap map, ClusterManager<PoiItem> clusterManager) {
        super(context, map, clusterManager);
        mContext = context;
        mClusterIconGenerator = new IconGenerator(context.getApplicationContext());

        clusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<PoiItem>() {
            @Override
            public boolean onClusterItemClick(PoiItem t) {
                return centerCameraOnClusterExpanded(map, t);
            }
        });
    }

    @Override
    protected void onBeforeClusterItemRendered(PoiItem item, MarkerOptions markerOptions) {
        if (item.getPoi().getTipology().equals(Filter.FILM_AND_GAMES.getId())) {
            generateMarkerWithText(markerOptions,R.drawable.marker_film_game ,String.valueOf(item.getPoi().getCount()));
        } else if (item.getPoi().getTipology().equals(Filter.GAME.getId())) {
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_game));

        } else if (item.getPoi().getTipology().equals(Filter.FILM.getId())) {
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_film));
        }
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<PoiItem> cluster, MarkerOptions markerOptions) {

        generateMarkerWithText( markerOptions,R.drawable.cluster_film_game, String.valueOf(cluster.getSize()));
    }

    private void generateMarkerWithText(MarkerOptions markerOptions, @DrawableRes int drawable, String text) {
        mClusterIconGenerator.setBackground(ContextCompat.getDrawable(mContext, drawable));
        mClusterIconGenerator.setTextAppearance(R.style.TextClusterManager);

        final Bitmap icon = mClusterIconGenerator.makeIcon(text);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster<PoiItem> cluster) {
        return cluster.getSize() > 1;
    }

    private boolean centerCameraOnClusterExpanded(GoogleMap map, ClusterItem item) {

        LatLngBounds.Builder builder = LatLngBounds.builder();
        builder.include(item.getPosition());
        final LatLngBounds bounds = builder.build();
        try {
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } catch (Exception error) {
            Log.e(TAG, error.getMessage());
        }

        return false;
    }
}
