package com.mobilesoft.italyformovies.ui;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public abstract class GoogleMapItem<T> implements ClusterItem {
    LatLng position;
    T item;

    public GoogleMapItem(T item) {
        this.item = item;
    }
}
