package com.mobilesoft.italyformovies.ui

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.util.Log
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.Marker
import com.mobilesoft.italyformovies.fragment.MapFragment


val Float.dp: Float
    get() = (this / Resources.getSystem().displayMetrics.density)

val Float.px: Float
    get() = (this * Resources.getSystem().displayMetrics.density)


fun GoogleMap.disableDefaultPois(context: Context?) {
    try {
        // Customise the styling of the base map using a JSON object defined
        // in a raw resource file.
        val success = this.setMapStyle(
            MapStyleOptions.loadRawResourceStyle(
                context, com.mobilesoft.italyformovies.R.raw.style_json
            )
        )

    } catch (e: Resources.NotFoundException) {
        Log.e(MapFragment.TAG, "Can't find style. Error: ", e)
    }


}

fun GoogleMap.centerCameraOnMarkerClollection(markers: List<Marker?>, padding: Int) {
    if (markers.isNotEmpty()){
        val builder = LatLngBounds.Builder()
        for (marker in markers) {
            builder.include(marker?.position)
        }
        val bounds = builder.build()
        this.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding))
    }

}

fun GoogleMap.centerCameraOnMarker(marker: Marker?, zoom: Float = 12.0f) {


    this.moveCamera(CameraUpdateFactory.newLatLngZoom(marker?.position, zoom))
}

fun Context.openBrowser(url: String?) {
    if (!url.isNullOrEmpty()) {

        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(browserIntent)
    }
}
