package com.mobilesoft.italyformovies.ui

import android.content.Context
import com.mobilesoft.italyformovies.R
import java.security.AccessControlContext


data class Region(val context: Context?, val id: Int) {

    val imageResource: Int
        get() = when (id) {
            Region.PIEMONTE.id -> R.drawable.piemonte

            Region.ABBRUZZO.id -> R.drawable.abruzzo
            Region.TRENTINO.id -> R.drawable.trentino_alto_adige
            Region.BASILICATA.id -> R.drawable.basilicata
            Region.CALABRIA.id -> R.drawable.calabria
            Region.CAMPANIA.id -> R.drawable.campania
            Region.EMILIA_ROMAGNA.id -> R.drawable.emilia_romagna
            Region.FRIULI_VENEZIA_GIULIA.id -> R.drawable.friuli_venezia_giulia
            Region.LAZIO.id -> R.drawable.lazio
            Region.LIGURIA.id -> R.drawable.liguria
            Region.LOMBARDIA.id -> R.drawable.lombardia
            Region.MARCHE.id -> R.drawable.marche
            Region.MOLISE.id -> R.drawable.molise
            Region.PUGLIA.id -> R.drawable.puglia
            Region.SARDEGNA.id -> R.drawable.sardegna
            Region.SICILIA.id -> R.drawable.sicilia
            Region.TOSCANA.id -> R.drawable.toscana
            Region.ALTO_ADIGE.id -> R.drawable.trentino_alto_adige
            Region.UMBRIA.id -> R.drawable.umbria
            Region.VALLE_AOSTA.id -> R.drawable.valledaosta
            Region.VENETO.id -> R.drawable.veneto

            else -> 0
        }

    val name: String?
        get() {
            var resId = 0
            when (id) {
                Region.ABBRUZZO.id -> resId = R.string.abruzzo
                Region.TRENTINO.id  -> resId = R.string.trentino_alto_adige
                Region.BASILICATA.id  -> resId = R.string.basilicata
                Region.CALABRIA.id  -> resId = R.string.calabria
                Region.CAMPANIA.id  -> resId = R.string.campania
                Region.EMILIA_ROMAGNA.id  -> resId = R.string.emilia_romagna
                Region.FRIULI_VENEZIA_GIULIA.id  -> resId = R.string.friuli_venezia_giulia
                Region.LAZIO.id  -> resId = R.string.lazio
                Region.LIGURIA.id  -> resId = R.string.liguria
                Region.LOMBARDIA.id  -> resId = R.string.lombardia
                Region.MARCHE.id  -> resId = R.string.marche
                Region.MOLISE.id  -> resId = R.string.molise
                Region.PIEMONTE.id  -> resId = R.string.piemonte
                Region.PUGLIA.id  -> resId = R.string.puglia
                Region.SARDEGNA.id  -> resId = R.string.sardegna
                Region.SICILIA.id  -> resId = R.string.sicilia
                Region.TOSCANA.id  -> resId = R.string.toscana
                Region.ALTO_ADIGE.id  -> resId = R.string.alto_adige
                Region.UMBRIA.id  -> resId = R.string.umbria
                Region.VALLE_AOSTA.id  -> resId = R.string.valledaosta
                Region.VENETO.id  -> resId = R.string.veneto
            }

            return context?.resources?.getString(resId)
        }

    enum class Region(val id: Int) {
        ABBRUZZO(13),
        ALTO_ADIGE(21),
        BASILICATA(17),
        CALABRIA(18),
        CAMPANIA(15),
        EMILIA_ROMAGNA(8),
        FRIULI_VENEZIA_GIULIA(6),
        LAZIO(12),
        LIGURIA(7),
        LOMBARDIA(3),
        MARCHE(11),
        MOLISE(14),
        PIEMONTE(1),
        PUGLIA(16),
        SARDEGNA(20),
        SICILIA(19),
        TOSCANA(9),
        TRENTINO(4),
        UMBRIA(10),
        VALLE_AOSTA(2),
        VENETO(5),
    }
}


