package com.mobilesoft.italyformovies.ui

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class MenuItem(@DrawableRes val image: Int, @StringRes val title: Int , val tag: String)
