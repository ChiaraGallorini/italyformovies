package com.mobilesoft.italyformovies.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.mobilesoft.italyformovies.R
import kotlinx.android.synthetic.main.header_view.view.*

class Header @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

     var title: String? = null

    init {

        val typedArray = context.obtainStyledAttributes(
                attrs, R.styleable.Header
        )
        title = typedArray.getString(R.styleable.Header_title)
        val isSearchBarVisible = typedArray.getBoolean(R.styleable.Header_isSearchBar, false)

        typedArray.recycle()

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                as LayoutInflater
        inflater.inflate(R.layout.header_view, this)

        tv_header.text = title

        if (isSearchBarVisible) {
            tv_header.visibility = View.GONE
             et_search_view.visibility = View.VISIBLE
            iv_filter.visibility = View.VISIBLE

        } else {
            tv_header.visibility = View.VISIBLE
            et_search_view.visibility = View.GONE
            iv_filter.visibility = View.GONE
        }
    }
}
