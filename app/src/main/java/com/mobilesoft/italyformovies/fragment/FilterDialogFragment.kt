package com.mobilesoft.italyformovies.fragment

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.mobilesoft.data.model.Filter
import com.mobilesoft.italyformovies.R
import kotlinx.android.synthetic.main.filter_dialog_fragment.view.*

class FilterDialogFragment : DialogFragment() {

    var mPoiListener: ((Filter?) -> Unit)? = null
    var mIncentiveCheckedListener: ((Boolean) -> Unit)? = null
    var mSelectedFilter: Filter? = null
    var shouldInvokeListener: Boolean = false


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {


        val position = arguments!!.getIntArray(ARG_SCREEN_POSITION)

        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCanceledOnTouchOutside(true)
        val wmlp = dialog.window!!.attributes
        wmlp.x = 0  //x position
        wmlp.y = -500
        return dialog
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        shouldInvokeListener = false

        val view = inflater.inflate(R.layout.filter_dialog_fragment, container)
        view.rg_filters.setOnCheckedChangeListener { group, checkedId ->
            if (shouldInvokeListener) {


                when (checkedId) {
                    R.id.rb_film_and_game -> mSelectedFilter = Filter.FILM_AND_GAMES
                    R.id.rb_location -> mSelectedFilter = Filter.LOCATION
                }
                mPoiListener?.invoke(mSelectedFilter)
                this.dismiss()
            }


        }

        view.sw_filter_incentive.setOnCheckedChangeListener { buttonView, isChecked ->
            if (shouldInvokeListener) {
                mIncentiveCheckedListener?.invoke(isChecked)
                this.dismiss()
            }
        }

        return view
    }

    override fun onResume() {
        super.onResume()

        shouldInvokeListener = true
    }


    fun setOnFilterChangedListener(listener: (Filter?) -> Unit) {
        mPoiListener = listener
    }

    fun setOnIncentiveCheckedListener(listener: (Boolean) -> Unit) {
        mIncentiveCheckedListener = listener
    }

    companion object {

        private val TAG = FilterDialogFragment::class.java.simpleName
        private val ARG_SCREEN_POSITION = "screenPosition"


        fun newInstance(coordinate: IntArray): FilterDialogFragment {
            val frag = FilterDialogFragment()
            val args = Bundle()
            args.putIntArray(ARG_SCREEN_POSITION, coordinate)
            frag.arguments = args
            return frag
        }
    }
}