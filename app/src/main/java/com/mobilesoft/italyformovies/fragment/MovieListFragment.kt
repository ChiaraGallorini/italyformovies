package com.mobilesoft.italyformovies.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobilesoft.data.model.Movie
import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.adapter.MovieRecyclerViewAdapter
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener.Command
import com.mobilesoft.italyformovies.ui.SpacesItemDecoration
import com.mobilesoft.italyformovies.ui.px
import kotlinx.android.synthetic.main.fragment_movie_list.view.*


class MovieListFragment : AppFragment() {

    companion object {

        val TAG: String = MovieListFragment::class.java.simpleName
    }


    private var mMoviesAdapter: MovieRecyclerViewAdapter? = null
    private var mCurrentPage: Int = 1
    private var mTotalRequestPage: Int = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.getMovieListData().observe(this,
            Observer<List<Movie>> { movies ->
                mTotalRequestPage++
                Log.d("MovieListFragment", "Total request page is $mTotalRequestPage")
                mMoviesAdapter?.addItems(movies)
                mListener?.onCommand(Command.HideLoader)
            })


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_movie_list, container, false)
        val gridLayoutManager = GridLayoutManager(context, 3)
        with(view.rv_movie_list) {
            layoutManager = gridLayoutManager
            mMoviesAdapter = MovieRecyclerViewAdapter(context) { movie ->
                mViewModel.selectedMovieId = movie.id
                mListener?.onCommand(Command.ShowMovieDetail)
            }
            adapter = mMoviesAdapter

        }

        view.rv_movie_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) { // only when scrolling up

                    val visibleThreshold = 2

                    val layoutManager = recyclerView.layoutManager as GridLayoutManager;
                    val lastItem = layoutManager.findLastCompletelyVisibleItemPosition();
                    val currentTotalCount = layoutManager.itemCount;

                    if (currentTotalCount <= lastItem + visibleThreshold) {
                        if (mCurrentPage <= mTotalRequestPage) {
                            mCurrentPage++
                            Log.d("MovieListFragment", "Current page is $mCurrentPage")
                            mViewModel.fetchMovieListsData(mCurrentPage)
                        }
                    }
                }
            }
        })
        return view
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume $TAG")
        mListener?.onCommand(Command.TrackFragment(TAG))
        mListener?.onCommand(Command.ShowLoader)
        mViewModel.fetchMovieListsData(mCurrentPage)
    }
}
