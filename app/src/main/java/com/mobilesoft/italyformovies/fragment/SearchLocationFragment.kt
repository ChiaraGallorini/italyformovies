package com.mobilesoft.italyformovies.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener.Command



class SearchLocationFragment : AppFragment() {

    companion object {

        val TAG: String = SearchLocationFragment::class.java.simpleName
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_location, container, false)
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume $TAG")
        mListener?.onCommand(Command.TrackFragment(TAG))
    }
}
