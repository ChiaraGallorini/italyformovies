package com.mobilesoft.italyformovies.fragment

import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.mobilesoft.data.model.Incentive
import com.mobilesoft.data.model.News

import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener.Command
import com.mobilesoft.italyformovies.ui.openBrowser
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_incentive_detail.*
import kotlinx.android.synthetic.main.fragment_incentive_detail.view.*
import kotlinx.android.synthetic.main.fragment_news_detail.*
import kotlinx.android.synthetic.main.fragment_news_detail.view.*
import java.text.DateFormatSymbols
import java.util.*


class IncentiveDetailFragment : AppFragment() {

    companion object {

        val TAG: String = IncentiveDetailFragment::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.getIncentiveDetailData().observe(this,
            Observer<Incentive> { incentive ->

                mListener?.onCommand(Command.HideLoader)

                tv_incentive_title.text = incentive?.title
                tv_incentive_content.text = Html.fromHtml(incentive?.content)
                tv_incentive_link_url.text = Html.fromHtml("<u>${incentive?.urlExternalLink}</u>")
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_incentive_detail, container, false)

        with(view) {
            bottom_navigation_incentives.setOnNavigationItemReselectedListener { item ->
                when (item.itemId) {
                    R.id.action_map -> mListener?.onCommand(Command.ShowMap)
                }
            }
            tv_incentive_link_url.setOnClickListener {
                context?.openBrowser(mViewModel.getIncentiveDetailData().value?.urlExternalLink)
            }

        }

        return view
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume $TAG")
        mListener?.onCommand(Command.TrackFragment(TAG))
        val incentiveId = mViewModel.selectedIncetiveId
        if (incentiveId != null) {

            mListener?.onCommand(Command.ShowLoader)
            mViewModel.fetchSelectedIncentive(incentiveId)

        }
    }
}
