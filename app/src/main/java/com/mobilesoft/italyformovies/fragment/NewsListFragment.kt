package com.mobilesoft.italyformovies.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobilesoft.data.model.News
import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.adapter.NewsRecyclerViewAdapter
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener.Command
import kotlinx.android.synthetic.main.fragment_news_list.view.*


class NewsListFragment : AppFragment() {

    companion object {

        val TAG: String = NewsListFragment::class.java.simpleName
    }

    private var mNewsAdapter : NewsRecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.getNewsListData().observe(this,
            Observer<List<News>> { news ->
                mNewsAdapter?.setItems(news)
                mListener?.onCommand(Command.HideLoader)
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_news_list, container, false)

        with(view.rv_news_list) {
            layoutManager = LinearLayoutManager(context)
            mNewsAdapter =  NewsRecyclerViewAdapter( context) { news ->
                mViewModel.selectedNewsId = news.id
                mListener?.onCommand(Command.ShowNewsDetail)
            }
            adapter = mNewsAdapter
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume $TAG")
        mListener?.onCommand(Command.TrackFragment(TAG))

        mListener?.onCommand(Command.ShowLoader)
        mViewModel.fetchNewsListData()
    }
}
