package com.mobilesoft.italyformovies.fragment

import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.mobilesoft.data.model.News

import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener.Command
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_news_detail.*
import kotlinx.android.synthetic.main.fragment_news_detail.view.*
import java.text.DateFormatSymbols
import java.util.*


class NewsDetailFragment : AppFragment() {

    companion object {

        val TAG: String = NewsDetailFragment::class.java.simpleName
    }


    private val mDateFromSymbols: DateFormatSymbols = DateFormatSymbols()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.getSelectedNewsData().observe(this,
            Observer<News> { news ->

                mListener?.onCommand(Command.HideLoader)
                val dateLong = news?.date;
                if (dateLong != null) {
                    val date = Date(dateLong)
                    tv_month.text = mDateFromSymbols.shortMonths[date.month]
                    tv_day.text = date.day.toString()
                }
                tv_news_content.text = Html.fromHtml(news?.description)
                tv_news_title.text = news?.title
                Picasso.with(context).load(news?.image).into(iv_news)
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_news_detail, container, false)

        with(view) {
            bottom_navigation_news.setOnNavigationItemReselectedListener { item ->
                when (item.itemId) {
                    R.id.action_map -> mListener?.onCommand(Command.ShowMap)
                }
            }
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume $TAG")
        mListener?.onCommand(Command.TrackFragment(TAG))

        val newsId = mViewModel.selectedNewsId
        if (newsId != null) {
            mListener?.onCommand(Command.ShowLoader)
            mViewModel.fetchSelectedNewsData(newsId)

        }
    }
}
