package com.mobilesoft.italyformovies.fragment

import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import androidx.lifecycle.Observer
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.mobilesoft.data.model.Movie
import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.adapter.ImagePagerAdapter
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener.Command
import com.mobilesoft.italyformovies.ui.centerCameraOnMarkerClollection
import com.mobilesoft.italyformovies.ui.disableDefaultPois
import com.mobilesoft.italyformovies.ui.px
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_movie_detail.*
import kotlinx.android.synthetic.main.fragment_movie_detail.view.*


class MovieDetailFragment : AppFragment(), OnMapReadyCallback {

    companion object {

        val TAG: String = MovieDetailFragment::class.java.simpleName
    }

    private var mGoogleMap: GoogleMap? = null
    private var mMarkerCollecion: MutableList<Marker?> = mutableListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val iconLocation = BitmapDescriptorFactory.fromResource(R.drawable.marker_location)

        mViewModel.getSelectedMovieData().observe(this,
            Observer<Movie> { movie ->

                mListener?.onCommand(Command.HideLoader)


                Picasso.with(context).load(movie?.cover).into(iv_movie_cover)
                tv_movie_genre.text = movie.genre
                tv_movie_director.text = movie.director
                tv_movie_prodcution_country.text = movie.country
                tv_movie_cast.text = movie.actors
                tv_movie_year.text = movie.productionYear
                tv_movie_production.text = Html.fromHtml(movie.productionName)
                tv_movie_description.text = Html.fromHtml(movie.description)
                vp_movie_images.adapter = ImagePagerAdapter(context, movie.images)
                tv_movie_title.text = movie.title
                web_view_movie.loadData(
                    "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/${movie.urlVideo}\" frameborder=\"0\" allowfullscreen></iframe>"
                    , "text/html", "utf-8"
                )


                movie.locations.forEach { location ->
                    if (location!= null){
                        val marker = mGoogleMap?.addMarker(
                            MarkerOptions().position(
                                LatLng(
                                    location.latitude,
                                    location.longitude
                                )
                            ).icon(iconLocation)
                        )
                        mMarkerCollecion.add(marker)
                    }
                }
                mGoogleMap?.centerCameraOnMarkerClollection(mMarkerCollecion, 50f.px.toInt())
            })
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_movie_detail, container, false)
        with(view.web_view_movie) {
            settings.javaScriptEnabled = true
            webChromeClient = WebChromeClient()

        }

        view.map_view_movie_location.apply {
            onCreate(savedInstanceState)
            onResume()
            getMapAsync(this@MovieDetailFragment)
        }


        return view
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume $TAG")
        mListener?.onCommand(Command.TrackFragment(TAG))

        val movieId = mViewModel.selectedMovieId
        if (movieId != null) {
            mListener?.onCommand(Command.ShowLoader)

            mViewModel.fetchSelectedMovieData(movieId)
        }
    }

    override fun onMapReady(map: GoogleMap?) {
        mGoogleMap = map
        mGoogleMap?.uiSettings?.isMyLocationButtonEnabled = false
        mGoogleMap?.uiSettings?.isCompassEnabled = false
        mGoogleMap?.uiSettings?.setAllGesturesEnabled(false)
        mGoogleMap?.disableDefaultPois(context)
    }
}
