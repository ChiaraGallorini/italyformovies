package com.mobilesoft.italyformovies.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.mobilesoft.data.model.Game
import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.adapter.ImagePagerAdapter
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener.Command
import com.mobilesoft.italyformovies.ui.centerCameraOnMarkerClollection
import com.mobilesoft.italyformovies.ui.disableDefaultPois
import com.mobilesoft.italyformovies.ui.px
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_game_detail.*
import kotlinx.android.synthetic.main.fragment_game_detail.view.*


class GameDetailFragment : AppFragment(), OnMapReadyCallback {

    companion object {

        val TAG: String = GameDetailFragment::class.java.simpleName
    }

    private var mGoogleMap: GoogleMap? = null
    private var mMarkerCollecion: MutableList<Marker?> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val iconLocation = BitmapDescriptorFactory.fromResource(R.drawable.marker_location)

        mViewModel.getSelectedGameData().observe(this,
            Observer<Game> { game ->

                mListener?.onCommand(Command.HideLoader)


                Picasso.with(context).load(game?.cover).into(iv_game_cover)
                tv_game_genre.text = game.genre
                tv_game_year.text = game.productionYear
                tv_game_description.text = game.description
                vp_game_images.adapter = ImagePagerAdapter(context, game.images)
                tv_game_title.text = game.title
                tv_game_development.text = game.development


                if (game.locations.isNotEmpty()) {
                    game.locations.forEach { location ->
                        val marker = mGoogleMap?.addMarker(
                            MarkerOptions().position(
                                LatLng(
                                    location.latitude,
                                    location.longitude
                                )
                            ).icon(iconLocation)
                        )
                        mMarkerCollecion.add(marker)
                    }
                    mGoogleMap?.centerCameraOnMarkerClollection(mMarkerCollecion, 50f.px.toInt())
                }else{
                    map_view_game_location.visibility = View.GONE
                }
            })
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_game_detail, container, false)


        view.map_view_game_location.apply {
            onCreate(savedInstanceState)
            onResume()
            getMapAsync(this@GameDetailFragment)
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume $TAG")
        mListener?.onCommand(Command.TrackFragment(TAG))

        val gameId = mViewModel.selectedGameId
        if (gameId != null) {
            mListener?.onCommand(Command.ShowLoader)
            mViewModel.fetchSelectedGameData(gameId)
        }
    }

    override fun onMapReady(map: GoogleMap?) {
        mGoogleMap = map
        mGoogleMap?.uiSettings?.isMyLocationButtonEnabled = false
        mGoogleMap?.uiSettings?.isCompassEnabled = false
        mGoogleMap?.uiSettings?.setAllGesturesEnabled(false)
        mGoogleMap?.disableDefaultPois(context)
    }
}
