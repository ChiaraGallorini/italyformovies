package com.mobilesoft.italyformovies.fragment

import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.mobilesoft.data.model.Movie
import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener.Command
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_movie_detail.*


class SearchGameFragment : AppFragment() {

    companion object {

        val TAG: String = SearchGameFragment::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.getSelectedMovieData().observe(this,
            Observer<Movie> { movie ->

                mListener?.onCommand(Command.HideLoader)

                Picasso.with(context).load(movie?.cover).into(iv_movie_cover)
                tv_movie_genre.text = movie.genre
                tv_movie_director.text = movie.director
                tv_movie_prodcution_country.text = movie.country
                tv_movie_cast.text = movie.actors
                tv_movie_year.text = movie.productionYear
                tv_movie_production.text = Html.fromHtml(movie.productionName)
                tv_movie_description.text =  Html.fromHtml(movie.description)
                tv_movie_title.text = movie.title
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_game_search, container, false)


        return view
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume $TAG")
        mListener?.onCommand(Command.TrackFragment(TAG))

        val movieId = mViewModel.selectedMovieId
        if (movieId != null) {
            mListener?.onCommand(Command.ShowLoader)

            mViewModel.fetchSelectedMovieData(movieId)

        }
    }
}
