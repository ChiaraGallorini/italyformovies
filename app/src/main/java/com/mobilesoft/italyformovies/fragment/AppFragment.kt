package com.mobilesoft.italyformovies.fragment

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener
import com.mobilesoft.italyformovies.viewmodel.MainViewModel


open class AppFragment : Fragment() {

    protected var mListener: OnFragmentInteractionListener? = null

    lateinit var mViewModel: MainViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(activity!!).get(MainViewModel::class.java)
    }

    fun showDialog(title: String?, message: String?, buttonText: String?) {
        val alertDialogBuilder = AlertDialog.Builder(context)
        alertDialogBuilder.setTitle(title ?: "")
        alertDialogBuilder
            .setMessage(message ?: "-")
            .setCancelable(false)
            .setPositiveButton(buttonText ?: getString(R.string.confirm)) { dialog, id -> dialog.dismiss() }

        val alertDialog = alertDialogBuilder.create()

        alertDialog.show()
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }
}
