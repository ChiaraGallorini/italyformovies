package com.mobilesoft.italyformovies.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobilesoft.data.model.Incentive
import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.adapter.IncentiveRecyclerViewAdapter
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener.Command
import kotlinx.android.synthetic.main.fragment_incentive_list.view.*


class IncentiveListFragment : AppFragment() {


    companion object {
        val TAG = IncentiveListFragment::class.java.simpleName
    }

    private var mIncentivesAdapter: IncentiveRecyclerViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.getIncentivesData().observe(this,
            Observer<List<Incentive>> { incetives ->
                mIncentivesAdapter?.setItems(incetives)
                mListener?.onCommand(Command.HideLoader)

            })
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_incentive_list, container, false)
        with(view.rv_incentive_list) {
            layoutManager = LinearLayoutManager(context)
            mIncentivesAdapter = IncentiveRecyclerViewAdapter { incentive ->
                mViewModel.selectedIncetiveId = incentive.id
                mListener?.onCommand(Command.ShowIncentiveDetail)
            }
            adapter = mIncentivesAdapter
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume $TAG")
        mListener?.onCommand(Command.TrackFragment(TAG))
        mListener?.onCommand(Command.ShowLoader)
        mViewModel.fetchIncentitvesData()
    }
}
