package com.mobilesoft.italyformovies.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import androidx.lifecycle.Observer
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.mobilesoft.data.model.Location
import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.adapter.ImagePagerAdapter
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener.Command
import com.mobilesoft.italyformovies.ui.centerCameraOnMarker
import com.mobilesoft.italyformovies.ui.disableDefaultPois
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_location_detail.*
import kotlinx.android.synthetic.main.fragment_location_detail.view.*
import android.content.Intent
import android.net.Uri
import android.text.Html
import android.util.Log
import com.mobilesoft.italyformovies.ui.Region
import com.mobilesoft.italyformovies.ui.openBrowser
import kotlinx.android.synthetic.main.fragment_incentive_detail.*
import kotlinx.android.synthetic.main.header_view.*


class LocationDetailFragment : AppFragment(), OnMapReadyCallback {


    companion object {

        val TAG: String = LocationDetailFragment::class.java.simpleName
    }

    private var mGoogleMap: GoogleMap? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val iconLocation = BitmapDescriptorFactory.fromResource(R.drawable.marker_location)

        mViewModel.getLocationDetailData().observe(this,
            Observer<Location> { location ->
                location_header.title = location.instagram

                val region = Region(context, location.region)

                mListener?.onCommand(Command.HideLoader)
                tv_location_address.text = Html.fromHtml("${location.address} <B>${region.name}</B>")
                tv_location_research_key.text = location.keysSearch.replace(",", " | ")
                tv_location_tipology.text = location.tipology
                tv_location_title.text = location.title

                val marker = mGoogleMap?.addMarker(
                    MarkerOptions().position(
                        LatLng(
                            location.latitude,
                            location.longitude
                        )
                    ).icon(iconLocation)
                )
                mGoogleMap?.centerCameraOnMarker(marker)

                Picasso.with(context).load(region.imageResource).into(iv_italy_region)
                tv_location_text.text = location.text

                vp_location_images.adapter = ImagePagerAdapter(context, location.images)
                web_view_location.loadData(
                    "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/${location.urlVideo}\" frameborder=\"0\" allowfullscreen></iframe>"
                    , "text/html", "utf-8"
                )
                if (location.water) iv_water.visibility = View.VISIBLE else View.INVISIBLE
                if (location.energyElectric) iv_light.visibility = View.VISIBLE else View.INVISIBLE

            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_location_detail, container, false)
        with(view.web_view_location) {
            settings?.javaScriptEnabled = true
            webChromeClient = WebChromeClient()

        }

        view.map_view_location.apply {
            onCreate(savedInstanceState)
            onResume()
            getMapAsync(this@LocationDetailFragment)
        }
        view.tv_film_commission.setOnClickListener {
            context?.openBrowser(mViewModel.getLocationDetailData().value?.urlFilmCommission)

        }

        view.tv_show_document.setOnClickListener {
            context?.openBrowser(mViewModel.getLocationDetailData().value?.urlLocationDocument)
        }

        view.bottom_navigation_location.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.action_map -> {
                    mListener?.onCommand(Command.ShowMap)
                }

            }
            return@setOnNavigationItemSelectedListener true
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume $TAG")
        mListener?.onCommand(Command.TrackFragment(TAG))

        val locationId = mViewModel.selectedLocationId
        if (locationId != null) {
            mListener?.onCommand(Command.ShowLoader)

            mViewModel.fetchSelectedLocation(locationId)

        }
    }

    override fun onMapReady(map: GoogleMap?) {
        mGoogleMap = map
        mGoogleMap?.uiSettings?.isMyLocationButtonEnabled = false
        mGoogleMap?.uiSettings?.isCompassEnabled = false
        mGoogleMap?.uiSettings?.setAllGesturesEnabled(false)
        mGoogleMap?.disableDefaultPois(context)
    }
}
