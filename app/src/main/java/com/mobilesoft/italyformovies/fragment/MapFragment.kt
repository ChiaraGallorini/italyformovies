package com.mobilesoft.italyformovies.fragment

import android.Manifest
import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.Places
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.maps.android.clustering.ClusterItem
import com.google.maps.android.clustering.ClusterManager
import com.mobilesoft.data.model.Filter
import com.mobilesoft.data.model.Poi
import com.mobilesoft.data.model.PoisDetail
import com.mobilesoft.italyformovies.R

import com.mobilesoft.italyformovies.adapter.PoisPagerAdapter
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener.Command
import com.mobilesoft.italyformovies.ui.*
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.fragment_map.*
import kotlinx.android.synthetic.main.fragment_map.view.*
import kotlinx.android.synthetic.main.header_view.view.*
import java.util.*


class MapFragment : AppFragment(), OnMapReadyCallback, ClusterManager.OnClusterItemClickListener<PoiItem?> {


    companion object {
        const val MY_PERMISSIONS_REQUEST_LOCATION = 99
        const val MAX_ZOOM_CAMERA = 18.0f
        const val MIN_ZOOM_CAMERA = 12.0f
        val TAG: String = MapFragment::class.java.simpleName
    }


    private var mGoogleMap: GoogleMap? = null

    private var mMoviesAndGamesItemClusterManager: ClusterManager<PoiItem>? = null
    private var mLocationItemClusterManager: ClusterManager<PoiItem>? = null
    private val mLocationItemsList: MutableList<PoiItem> by lazy { ArrayList<PoiItem>() }
    private val mMovieAndGameItemsList: MutableList<PoiItem> by lazy { ArrayList<PoiItem>() }
    private var mFilterDialog: FilterDialogFragment? = null
    private val mFilterDialogLocation = IntArray(2)

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mCurrentUserLocation: Location? = null
    var prevmarker: Marker? = null
    var prevmarkerGame: Marker? = null
    private var movieAndGamesRender: FilmAndGamesRender? = null
    private var locationRender: LocationClusterRender? = null
    private var isPagerShowUp: Boolean = false
    private var pagerAdapter: PoisPagerAdapter? = null
    private var previousSelectedMarker: Marker? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_map, container, false)
        view.btn_search_area.setOnClickListener {
            view.btn_search_area.visibility = View.INVISIBLE

            searchPoiInVisibleArea()
        }
        view.map_header.iv_filter.setOnClickListener {
            showFilterDialog()
        }


        view.ib_incentive.setOnClickListener {
            mListener?.onCommand(Command.ShowIncentives)
        }
        view.ib_center_user_location.setOnClickListener {
            centerUserPositionOnMap()
        }

        view.map_header.et_search_view.setOnClickListener {

        }
        view.btn_search_area.visibility = View.INVISIBLE


        view.google_map.apply {
            onCreate(savedInstanceState)
            onResume()
            getMapAsync(this@MapFragment)
        }


        view.vp_pois.translationY = resources.getDimension(R.dimen.pager_height).px
        pagerAdapter = PoisPagerAdapter(context)
        view.vp_pois.adapter = pagerAdapter
        view.vp_pois.pageMargin = 48
        view.vp_pois.clipToPadding = false

        return view
    }

    private fun showLocations() {
        mLocationItemClusterManager?.addItems(mLocationItemsList)
        mLocationItemClusterManager?.cluster()
        mGoogleMap?.setOnMarkerClickListener(mLocationItemClusterManager)
        mGoogleMap?.setOnCameraIdleListener(mLocationItemClusterManager)
    }

    private fun showMoviesAndGames() {
        mMoviesAndGamesItemClusterManager?.addItems(mMovieAndGameItemsList)
        mMoviesAndGamesItemClusterManager?.cluster()
        mGoogleMap?.setOnMarkerClickListener(mMoviesAndGamesItemClusterManager)
        mGoogleMap?.setOnCameraIdleListener(mMoviesAndGamesItemClusterManager)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.i(TAG, "onCreate");
        super.onCreate(savedInstanceState)
        mViewModel.getPoisData().observe(this,
            Observer<List<Poi>> { pois ->
                clearMapData()

                pois.forEach { poi ->
                    val poiItem = PoiItem(poi)
                    when (poi.tipology) {
                        Filter.FILM.id, Filter.FILM_AND_GAMES.id, Filter.GAME.id -> mMovieAndGameItemsList.add(poiItem)
                        Filter.LOCATION.id -> mLocationItemsList.add(poiItem)
                    }
                }

                if (mViewModel.poiFilter == Filter.FILM_AND_GAMES.id) {
                    showMoviesAndGames()
                    ib_incentive?.visibility =  View.VISIBLE
                } else if (mViewModel.poiFilter == Filter.LOCATION.id) {

                    showLocations()

                }
                mListener?.onCommand(Command.HideLoader)
            })
        checkLocationPermission()
        activity?.let {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(it)

        }



        mViewModel.getPoiDetailData().observe(this, Observer<PoisDetail> { detail ->
            pagerAdapter?.setItems(detail)
            popupAnimation(vp_pois)
            mListener?.onCommand(Command.HideLoader)


        })


    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume $TAG")
        mListener?.onCommand(Command.TrackFragment(TAG))

        btn_search_area.requestFocus()
    }

    override fun onPause() {
        super.onPause()
        Log.i(TAG, "onPause")

    }

    override fun onStop() {
        super.onStop()
        Log.i(TAG, "onStop")

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "onDestroy")

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mGoogleMap = googleMap
        mGoogleMap?.uiSettings?.isMyLocationButtonEnabled = false
        mGoogleMap?.uiSettings?.isCompassEnabled = true
        mGoogleMap?.setMinZoomPreference(MIN_ZOOM_CAMERA)
        mGoogleMap?.setMaxZoomPreference(MAX_ZOOM_CAMERA)
        mGoogleMap?.setOnCameraMoveStartedListener {
            reversePopupAnimation(vp_pois)
        }
        mMoviesAndGamesItemClusterManager = ClusterManager(
            context,
            mGoogleMap
        )
        mLocationItemClusterManager = ClusterManager(
            context,
            mGoogleMap
        )

        mGoogleMap?.disableDefaultPois(context)
        getCurrentUserPosition()


        locationRender = LocationClusterRender(
            context,
            mGoogleMap,
            mLocationItemClusterManager
        )
        mLocationItemClusterManager?.renderer = locationRender


        movieAndGamesRender = FilmAndGamesRender(
            context,
            mGoogleMap,
            mMoviesAndGamesItemClusterManager
        )
        mMoviesAndGamesItemClusterManager?.renderer = movieAndGamesRender
        mLocationItemClusterManager?.setOnClusterItemClickListener(this@MapFragment)
        mMoviesAndGamesItemClusterManager?.setOnClusterItemClickListener(this@MapFragment)


    }

    override fun onClusterItemClick(item: PoiItem?): Boolean {
        onClusterClicked(item)
        return false
    }

    private fun onClusterClicked(poiItem: PoiItem?): Boolean {

            var drawable = R.drawable.marker_film_game_ok
            var drawableSelectedMarker = R.drawable.marker_film_game_ok
            btn_search_area.visibility = View.VISIBLE

            var marker: Marker? = null


            when (poiItem?.poi?.tipology) {
                Filter.GAME.id -> {
                    prevmarkerGame?.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.marker_game))
                    marker = movieAndGamesRender?.getMarker(poiItem)
                    drawable = R.drawable.marker_game_ok
                    prevmarkerGame=movieAndGamesRender?.getMarker(poiItem)
                    drawableSelectedMarker = R.drawable.marker_game
                    if (previousSelectedMarker == null){
                        previousSelectedMarker = movieAndGamesRender?.getMarker(poiItem)

                    }
                }
            Filter.FILM.id -> {
                prevmarkerGame?.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.marker_film))
                marker = movieAndGamesRender?.getMarker(poiItem)
                prevmarkerGame=movieAndGamesRender?.getMarker(poiItem)
                drawable = R.drawable.marker_film_ok
                drawableSelectedMarker = R.drawable.marker_film
                if (previousSelectedMarker == null){
                    previousSelectedMarker = movieAndGamesRender?.getMarker(poiItem)

                }


            }
            Filter.LOCATION.id -> {
                prevmarker?.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.marker_location))
                marker = locationRender?.getMarker(poiItem)
                prevmarker = locationRender?.getMarker(poiItem)
                drawable = R.drawable.marker_location_ok
                drawableSelectedMarker = R.drawable.marker_location
                if (previousSelectedMarker == null){
                    previousSelectedMarker = locationRender?.getMarker(poiItem)

                }
            }
        }

        marker?.setIcon(BitmapDescriptorFactory.fromResource(drawable))

        return getPoiDetail(poiItem)
    }

    private fun getPoiDetail(item: PoiItem?): Boolean {
        mListener?.onCommand(Command.ShowLoader)
        mViewModel.fetchPoisDetailData(item?.poi?.id ?: 0)
        return centerCameraOnClusterExpanded(item)
    }

    private fun searchPoiInVisibleArea() {

        val bounds = mGoogleMap?.projection?.visibleRegion?.latLngBounds
        val northEast = bounds?.northeast
        val southwest = bounds?.southwest
        mListener?.onCommand(Command.ShowLoader)
        if (northEast != null && southwest != null) {
            mViewModel.fetchPoisData(
                northEast.latitude, northEast.longitude, southwest.latitude, southwest.longitude, mViewModel.poiFilter
            )

        } else {
            mListener?.onCommand(Command.ShowError(getString(R.string.retrieve_area_bound_error_message)))
        }

    }

    private fun clearMapData() {
        mMoviesAndGamesItemClusterManager?.clearItems()
        mLocationItemClusterManager?.clearItems()
        mLocationItemsList.clear()
        mMovieAndGameItemsList.clear()
        mGoogleMap?.clear()
    }

    private fun checkLocationPermission(): Boolean {
        context?.let {
            if (ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        activity!!
                        ,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                ) {


                    AlertDialog.Builder(it)
                        .setTitle(R.string.title_location_permission)
                        .setMessage(R.string.text_location_permission)
                        .setPositiveButton(R.string.ok) { dialogInterface, i ->
                            ActivityCompat.requestPermissions(
                                activity!!,
                                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                MY_PERMISSIONS_REQUEST_LOCATION
                            )
                        }
                        .create()
                        .show()


                } else {
                    ActivityCompat.requestPermissions(
                        activity!!,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        MY_PERMISSIONS_REQUEST_LOCATION
                    )
                }
                return false
            } else {
                return true
            }
        }
        return false
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getCurrentUserPosition()

                }
            }
        }

    }

    private fun getCurrentUserPosition() {
        context?.let { ctx ->
            if (ContextCompat.checkSelfPermission(
                    ctx
                    ,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                mGoogleMap?.let { map ->
                    map.isMyLocationEnabled = true
                }

                mFusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
                    if (location != null) {
                        mCurrentUserLocation = location
                        searchPoiInVisibleArea()
                        centerUserPositionOnMap()


                    }
                }

            }
        }
    }

    private fun centerUserPositionOnMap() {

        mCurrentUserLocation.let { loc ->
            if (loc?.latitude != null) {
                mGoogleMap?.animateCamera(

                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(
                            loc.latitude,
                            loc.longitude
                        ), MIN_ZOOM_CAMERA
                    )
                )
            }
        }
    }

    private fun popupAnimation(view: View) {
        if (!isPagerShowUp) {
            val showUpAnimation = ObjectAnimator.ofFloat(view, "translationY", view.height.toFloat().px, 0.0f)
            showUpAnimation?.apply {
                duration = 700
            }
            showUpAnimation.addListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {
                }

                override fun onAnimationEnd(animation: Animator?) {
                }

                override fun onAnimationCancel(animation: Animator?) {

                }

                override fun onAnimationStart(animation: Animator?) {
                    isPagerShowUp = true

                }

            })
            showUpAnimation.start()
        }
    }

    private fun reversePopupAnimation(view: View) {
        if (isPagerShowUp) {


            val reversePopupAnimation = ObjectAnimator.ofFloat(view, "translationY", 0.0f, view.height.toFloat().px)
            reversePopupAnimation?.apply {
                duration = 700
            }
            reversePopupAnimation.addListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {
                }

                override fun onAnimationEnd(animation: Animator?) {
                }

                override fun onAnimationCancel(animation: Animator?) {


                }

                override fun onAnimationStart(animation: Animator?) {
                    isPagerShowUp = false

                }

            })
            reversePopupAnimation.start()
        }
    }

    private fun showFilterDialog() {


        if (mFilterDialog == null) {
            mFilterDialog = FilterDialogFragment.newInstance(
                mFilterDialogLocation
            )
            mFilterDialog?.setOnFilterChangedListener { filter ->
                Log.i(TAG, "Filter changed ${filter?.name}")
                if (filter?.id != null && mViewModel.poiFilter != filter?.id) {
                    mViewModel.poiFilter = filter?.id
                    searchPoiInVisibleArea()

                }

            }
            mFilterDialog?.setOnIncentiveCheckedListener { checked ->
                Log.i(TAG, "Incentive  visibility =   $checked")
                ib_incentive.visibility = if (checked) View.VISIBLE else View.GONE
            }
        }
        if (fragmentManager != null) {
            mFilterDialog?.show(fragmentManager!!, "filter")

        }
    }

    private fun centerCameraOnClusterExpanded(item: ClusterItem?): Boolean {
        val builder = LatLngBounds.builder()
        builder.include(item?.position)
        val bounds = builder.build()
        try {
            mGoogleMap?.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
        } catch (error: Exception) {
            Log.e(TAG, error.message)
        }

        return false
    }
}






