package com.mobilesoft.italyformovies.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobilesoft.data.model.Game
import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.adapter.GameRecyclerViewAdapter
import com.mobilesoft.italyformovies.adapter.MovieRecyclerViewAdapter
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener.Command
import com.mobilesoft.italyformovies.ui.SpacesItemDecoration
import com.mobilesoft.italyformovies.ui.px
import kotlinx.android.synthetic.main.fragment_game_list.*
import kotlinx.android.synthetic.main.fragment_game_list.view.*
import kotlinx.android.synthetic.main.fragment_movie_list.view.*


class GameListFragment : AppFragment() {


    companion object {

        val TAG: String = GameListFragment::class.java.simpleName
    }

    private var mGamesAdapter: GameRecyclerViewAdapter? = null
    private var mCurrentPage: Int = 1
    private var mTotalRequestPage: Int = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.getGamesListData().observe(this,
            Observer<List<Game>> { games ->
                mTotalRequestPage++
                Log.d("GameListFragment", "Total request page is $mTotalRequestPage")
                mGamesAdapter?.addItems(games)
                mListener?.onCommand(Command.HideLoader)
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_game_list, container, false)
        val gridLayoutManager = GridLayoutManager(context, 3)
        with(view.rv_game_list) {
            layoutManager = gridLayoutManager
            mGamesAdapter = GameRecyclerViewAdapter(context) { game ->
                mViewModel.selectedGameId = game.id

                mListener?.onCommand(Command.ShowGameDetail)

            }

            adapter = mGamesAdapter
        }

        view.rv_game_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) { // only when scrolling up

                    val visibleThreshold = 2
                    val layoutManager = recyclerView.layoutManager as GridLayoutManager;
                    val lastItem = layoutManager.findLastCompletelyVisibleItemPosition();
                    val currentTotalCount = layoutManager.itemCount

                    if (currentTotalCount <= lastItem + visibleThreshold) {
                        if (mCurrentPage <= mTotalRequestPage) {
                            mCurrentPage++
                            Log.d("GameListFragment", "Current page is $mCurrentPage")
                            mViewModel.fetchMovieListsData(mCurrentPage)
                        }
                    }
                }
            }
        })
        return view
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume $TAG")
        mListener?.onCommand(Command.TrackFragment(TAG))
        mListener?.onCommand(Command.ShowLoader)
        mViewModel.fetchGameListsData(mCurrentPage)
    }
}
