package com.mobilesoft.italyformovies.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import com.mobilesoft.italyformovies.R;
import com.squareup.picasso.Picasso;

public class LocationFragment extends Fragment {

    private static final String ARG_LOCATION_NAME = "locationName";
    private static final String ARG_LOCATION_STREET = "locationStreet";
    private static final String ARG_IMAGE_URL = "imageUrl";


    public static LocationFragment newInstance(String locationName,
                                               String locationStreet,
                                               String imageUrl
    ) {
        LocationFragment fragment = new LocationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_LOCATION_NAME, locationName);
        args.putString(ARG_LOCATION_STREET, locationStreet);
        args.putString(ARG_IMAGE_URL, imageUrl);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_location, container, false);
        TextView texLocation = rootView.findViewById(R.id.tv_location);
        TextView textRegion = rootView.findViewById(R.id.tv_region);
        ImageView imageBackground = rootView.findViewById(R.id.iv_locations);
        String locationName = getArguments().getString(ARG_LOCATION_NAME, "-");
        String locationStreet = getArguments().getString(ARG_LOCATION_STREET, "-");
        String imageUrl = getArguments().getString(ARG_IMAGE_URL, "-");

        texLocation.setText(locationName);
        textRegion.setText(locationStreet);

        Picasso.with(getContext()).load(imageUrl).into(imageBackground);

        return rootView;
    }
}