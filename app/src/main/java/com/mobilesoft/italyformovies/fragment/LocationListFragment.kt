package com.mobilesoft.italyformovies.fragment


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mobilesoft.data.model.Location
import com.mobilesoft.italyformovies.R
import com.mobilesoft.italyformovies.adapter.LocationRecyclerViewAdapter
import com.mobilesoft.italyformovies.listener.OnFragmentInteractionListener.Command

import kotlinx.android.synthetic.main.fragment_location_list.view.*

class LocationListFragment : AppFragment() {

    companion object {

        val TAG: String = LocationListFragment::class.java.simpleName
    }

    private var mLocationsAdapter: LocationRecyclerViewAdapter? = null
    private var mCurrentPage: Int = 1
    private var mTotalRequestPage: Int = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.getLocationsData().observe(this,
            Observer<List<Location>> { locations ->
                mTotalRequestPage ++
                Log.d("MovieListFragment", "Total request page is $mTotalRequestPage")
                mLocationsAdapter?.addItems(locations)
                mListener?.onCommand(Command.HideLoader)

            })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_location_list, container, false)

        with(view.rv_location_list) {
            layoutManager = LinearLayoutManager(context)
            mLocationsAdapter = LocationRecyclerViewAdapter(context) { location ->
                mViewModel.selectedLocationId = location.id
                mListener?.onCommand(Command.ShowLocationDetail)
            }
            adapter = mLocationsAdapter
        }

        view.rv_location_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if(dy > 0){ // only when scrolling up

                    val visibleThreshold = 2

                    val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                    val lastItem  = layoutManager.findLastCompletelyVisibleItemPosition()
                    val currentTotalCount = layoutManager.itemCount

                    if(currentTotalCount <= lastItem + visibleThreshold){
                        if (mCurrentPage <= mTotalRequestPage){
                            mCurrentPage ++
                            Log.d("LocationListFragment", "Current page is $mCurrentPage")
                            mViewModel.fetchLocationsData(mCurrentPage)

                        }
                    }
                }
            }
        })

        return view
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume $TAG")
        mListener?.onCommand(Command.TrackFragment(TAG))
        mListener?.onCommand(Command.ShowLoader)
        mViewModel.fetchLocationsData(mCurrentPage)
    }
}
