package com.mobilesoft.italyformovies.listener

interface OnFragmentInteractionListener {

    sealed class Command {
        object ShowLoader : Command()
        object HideLoader : Command()
        object ShowMap : Command()
        // object ShowSearchLocation : Command()
        object ShowLocationDetail : Command()
        object ShowIncentives : Command()
object  ShowPlaces:Command()
        object ShowNewsDetail : Command()
        object ShowIncentiveDetail : Command()
        object ShowMovieDetail : Command()
        object ShowGameDetail : Command()
        class ShowError(val errorMessage: String) : Command()
        class TrackFragment(val fragmentTag: String) : Command()
    }

    fun onCommand(command: Command)
}