package com.mobilesoft.italyformovies.listener

interface OnItemSelectedListener<T> {
    fun onItemSelected(item: T)
}