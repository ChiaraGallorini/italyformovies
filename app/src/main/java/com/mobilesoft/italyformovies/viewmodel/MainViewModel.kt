package com.mobilesoft.italyformovies.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.mobilesoft.data.model.*
import com.mobilesoft.data.repository.RepositoryImpl


class MainViewModel(application: Application) : AndroidViewModel(application) {


    val predictions: MutableList<Predictions> = ArrayList()

    private val repository = RepositoryImpl.get()

    var selectedNewsId: Int? = null
    var selectedMovieId: Int? = null
    var selectedGameId: Int? = null
    var selectedLocationId: Int? = null
    var selectedIncetiveId: Int? = null
    var poiFilter: String? = null


    //******** GAME ********

    fun getGamesListData(): LiveData<List<Game>> = repository.gameListData

    fun getSelectedGameData(): LiveData<Game> = repository.selectedGameData

    fun fetchGameListsData(page: Int) {
        repository.fetchGameListData(page)
    }

    fun fetchSelectedGameData(id: Int) {
        repository.fetchSelectedGameData(id)
    }


    //******** MOVIE ********

    fun getSelectedMovieData(): LiveData<Movie> = repository.selectedMovieData

    fun getMovieListData(): LiveData<List<Movie>> = repository.movieListData


    fun fetchMovieListsData(page: Int) {
        repository.fetchMovieListData(page)
    }

    fun fetchSelectedMovieData(id: Int) {
        repository.fetchMovieData(id)
    }


    //******** NEWS ********

    fun getNewsListData(): LiveData<List<News>> = repository.newsListData

    fun getSelectedNewsData(): LiveData<News> = repository.selectedNewsData

    fun fetchSelectedNewsData(id: Int) {
        repository.fetchSelectedNewsData(id)
    }

    fun fetchNewsListData() {
        repository.fetchListNewsData()
    }


    //******** POIS ********

    fun fetchPoisData(
        northEstLatitude: Double,
        northEstLongitude: Double,
        southWestLatitude: Double,
        southWestLongitude: Double,
        filter: String?
    ) {
        repository.fetchPoisData(northEstLatitude, northEstLongitude, southWestLatitude, southWestLongitude, filter)
    }

    fun fetchPoisDetailData(id: Int) {
        repository.fetchPoiDetailData(id)
    }

    fun getPoisData(): LiveData<List<Poi>> = repository.poisData


    fun getPoiDetailData(): LiveData<PoisDetail> = repository.poiDetail

    //******** LOCATION ********


    fun fetchLocationsData(page: Int) {
        repository.fetchLocationsData(page)
    }

    fun fetchSelectedLocation(id: Int) {
        repository.fetchLocationDetailData(id)
    }

    fun getLocationsData(): LiveData<List<Location>> = repository.locationsData

    fun getLocationDetailData(): LiveData<Location> = repository.locationDetailData


    //******** INCENTIVE ********

    fun fetchIncentitvesData() {
        repository.fetchIncentitvesData()
    }

    fun fetchSelectedIncentive(id: Int) {
        repository.fetchIncentiveDetailData(id)
    }

    fun getIncentivesData(): LiveData<List<Incentive>> = repository.incentivesData

    fun getIncentiveDetailData(): LiveData<Incentive> = repository.incentiveDetailData

    fun getError(): LiveData<String> = repository.error
}

