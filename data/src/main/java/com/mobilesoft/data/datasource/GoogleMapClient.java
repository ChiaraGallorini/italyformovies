package com.mobilesoft.data.datasource;

import com.mobilesoft.data.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GoogleMapClient {


    private static final String BASE_URL = "https://maps.googleapis.com/maps/api/";
    private static GoogleMapClient sInstance;

    private GoogleMapApi mApi;

    private GoogleMapClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        if (BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        } else {
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);

        }
        builder.networkInterceptors().add(interceptor);

        OkHttpClient client = builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)

                .build();

        mApi = retrofit.create(GoogleMapApi.class);

    }

    static GoogleMapClient get() {
        if (sInstance == null) {
            synchronized (GoogleMapClient.class) {
                if (sInstance == null) {
                    sInstance = new GoogleMapClient();
                }
            }
        }
        return sInstance;
    }

    GoogleMapApi api() {
        return mApi;
    }
}
