package com.mobilesoft.data.datasource;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.mobilesoft.data.model.*;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

public class DataSource implements RemoteDataSource {


    private final MutableLiveData<String> mServiceErrorData = new MutableLiveData<>();
    private final MutableLiveData<List<Poi>> mPoisData = new MutableLiveData<>();
    private final MutableLiveData<PoisDetail> mPoiDetailData = new MutableLiveData<>();
    private final MutableLiveData<Predictions> mPredictions = new MutableLiveData<>();
    private final MutableLiveData<Movie> mMovieData = new MutableLiveData<>();
    private final MutableLiveData<List<Movie>> mMoviesData = new MutableLiveData<>();
    private final MutableLiveData<List<Game>> mGamesData = new MutableLiveData<>();
    private final MutableLiveData<Game> mGameDetailData = new MutableLiveData<>();
    private final MutableLiveData<List<News>> mNewsData = new MutableLiveData<>();
    private final MutableLiveData<News> mNewsSelectedData = new MutableLiveData<>();
    private final MutableLiveData<List<Location>> mLocationsData = new MutableLiveData<>();
    private final MutableLiveData<Location> mLocationDetailData = new MutableLiveData<>();
    private final MutableLiveData<List<Incentive>> mIncentivesData = new MutableLiveData<>();
    private final MutableLiveData<Incentive> mIncentiveDetailData = new MutableLiveData<>();


    @Override
    public LiveData<List<Poi>> getPoisData() {
        return mPoisData;
    }

    @Override
    public LiveData<PoisDetail> getPoiDetailsData() {
        return mPoiDetailData;
    }

    @Override
    public LiveData<List<News>> getNewsData() {
        return mNewsData;
    }

    @Override
    public LiveData<List<Location>> getLocationsData() {
        return mLocationsData;
    }

    @Override
    public LiveData<Movie> getMovieData() {
        return mMovieData;
    }

    @Override
    public LiveData<News> getSelectedNews() {
        return mNewsSelectedData;
    }

    @Override
    public LiveData<List<Movie>> getMovieListData() {
        return mMoviesData;
    }

    @Override
    public LiveData<List<Game>> getGameListData() {
        return mGamesData;
    }

    @Override
    public LiveData<Game> getGameDetailData() {
        return mGameDetailData;
    }

    @Override
    public LiveData<Predictions> getPredictionsData() {
        return mPredictions;
    }

    @Override
    public LiveData<Location> getLocationDetailData() {
        return mLocationDetailData;
    }

    @Override
    public LiveData<String> getError() {
        return mServiceErrorData;
    }

    @Override
    public LiveData<List<Incentive>> getIncentivesData() {
        return mIncentivesData;
    }

    @Override
    public LiveData<Incentive> getIncentiveDetailData() {
        return mIncentiveDetailData;
    }


    public void fetchPois(double nordEstLatitude,
                          double nordEstLongitude,
                          double sudWestLatitude,
                          double sudWestLongitude,
                          String filter) {
        Call<PoisResponnse> call = ItalyForMoviesClient.get().api().getPois(nordEstLatitude, nordEstLongitude, sudWestLatitude, sudWestLongitude, filter);
        call.enqueue(new Callback<PoisResponnse>() {
            @Override
            public void onResponse(Call<PoisResponnse> call, Response<PoisResponnse> response) {
                if (response.isSuccessful()) {
                    if (mPoisData.getValue() != null) {
                        mPoisData.getValue().clear();
                    }
                    List<Poi> pois = response.body().getPois();
                    if (pois.isEmpty()) {
                        mServiceErrorData.setValue("Pois not found");

                    } else {
                        mPoisData.setValue(pois);

                    }

                } else {
                    mServiceErrorData.setValue(response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<PoisResponnse> call, Throwable t) {
                mServiceErrorData.setValue(t.getMessage());
            }
        });
    }

    public void fetchPoisDetail(int id) {
        Call<PoisDetail> call = ItalyForMoviesClient.get().api().getSelectedPoi(id);
        call.enqueue(new Callback<PoisDetail>() {
            @Override
            public void onResponse(Call<PoisDetail> call, Response<PoisDetail> response) {
                if (response.isSuccessful()) {
                    mPoiDetailData.postValue(response.body());

                } else {
                    mServiceErrorData.setValue(response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<PoisDetail> call, Throwable t) {
                mServiceErrorData.setValue(t.getMessage());
            }
        });
    }

    public void fetchLocations(int page) {
        Call<LocationsResponse> call = ItalyForMoviesClient.get().api().getLocations(page);
        call.enqueue(new Callback<LocationsResponse>() {
            @Override
            public void onResponse(Call<LocationsResponse> call, Response<LocationsResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null) {
                        mLocationsData.postValue(response.body().getLocations());
                    }

                } else {
                    mServiceErrorData.setValue(response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<LocationsResponse> call, Throwable t) {
                mServiceErrorData.setValue(t.getMessage());
            }
        });
    }


    public void fetchSelectedLocation(int id) {
        Call<Location> call = ItalyForMoviesClient.get().api().getSelectedLocation(id);
        call.enqueue(new Callback<Location>() {
            @Override
            public void onResponse(Call<Location> call, Response<Location> response) {
                if (response.isSuccessful()) {

                    if (response.body() != null) {
                        mLocationDetailData.postValue(response.body());
                    }

                } else {
                    mServiceErrorData.setValue(response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<Location> call, Throwable t) {
                mServiceErrorData.setValue(t.getMessage());
            }
        });
    }

    public void fetchPlaces(String input,
                            String types,
                            String language,
                            String key) {
        Call<Predictions> call = GoogleMapClient.get().api().getPlaces(input, types, language, key);
        call.enqueue(new Callback<Predictions>() {
            @Override
            public void onResponse(Call<Predictions> call, Response<Predictions> response) {
                if (response.isSuccessful()) {

                    mPredictions.postValue(response.body());

                } else {
                    mServiceErrorData.setValue(response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<Predictions> call, Throwable t) {
                mServiceErrorData.setValue(t.getMessage());
            }
        });
    }


    public void fetchMovie(int id) {
        Call<Movie> call = ItalyForMoviesClient.get().api().getSelectedMovie(id);
        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                if (response.isSuccessful()) {

                    mMovieData.postValue(response.body());

                } else {
                    mServiceErrorData.setValue(response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                mServiceErrorData.setValue(t.getMessage());
            }
        });
    }


    public void fetchMovies(int page) {
        Call<MovieResponse> call = ItalyForMoviesClient.get().api().getMovies(page);
        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response.isSuccessful()) {

                    MovieResponse movieResp = response.body();
                    if (movieResp != null) {
                        mMoviesData.postValue(movieResp.getMovies());
                    }

                } else {
                    mServiceErrorData.setValue(response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                mServiceErrorData.setValue(t.getMessage());
            }
        });
    }


    public void fetchGames(int page) {
        Call<GamesResponse> call = ItalyForMoviesClient.get().api().getGames(page);
        call.enqueue(new Callback<GamesResponse>() {
            @Override
            public void onResponse(Call<GamesResponse> call, Response<GamesResponse> response) {
                if (response.isSuccessful()) {

                    GamesResponse gamesResponse = response.body();
                    if (gamesResponse != null) {
                        mGamesData.postValue(gamesResponse.getGames());
                    }

                } else {
                    mServiceErrorData.setValue(response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<GamesResponse> call, Throwable t) {
                mServiceErrorData.setValue(t.getMessage());
            }
        });
    }

    public void fetchSelectedGame(int id) {
        Call<Game> call = ItalyForMoviesClient.get().api().getSelectedGame(id);
        call.enqueue(new Callback<Game>() {
            @Override
            public void onResponse(Call<Game> call, Response<Game> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        mGameDetailData.postValue(response.body());
                    }

                } else {
                    mServiceErrorData.setValue(response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<Game> call, Throwable t) {
                mServiceErrorData.setValue(t.getMessage());
            }
        });
    }


    public void fetchNews() {
        Call<NewsResponse> call = ItalyForMoviesClient.get().api().getNews();
        call.enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                if (response.isSuccessful()) {
                    NewsResponse newsResp = response.body();
                    if (newsResp != null) {
                        mNewsData.postValue(newsResp.getNews());
                    }


                } else {
                    mServiceErrorData.setValue(response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                mServiceErrorData.setValue(t.getMessage());
            }
        });
    }

    public void fetchSelectedNews(int id) {
        Call<News> call = ItalyForMoviesClient.get().api().getSelectedNews(id);
        call.enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response) {
                if (response.isSuccessful()) {

                    mNewsSelectedData.postValue(response.body());

                } else {
                    mServiceErrorData.setValue(response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<News> call, Throwable t) {
                mServiceErrorData.setValue(t.getMessage());
            }
        });
    }


    public void fetchIncentives() {
        Call<IncentiveResponse> call = ItalyForMoviesClient.get().api().getIncentives();
        call.enqueue(new Callback<IncentiveResponse>() {
            @Override
            public void onResponse(Call<IncentiveResponse> call, Response<IncentiveResponse> response) {
                if (response.isSuccessful()) {
                    IncentiveResponse resp = response.body();
                    if (resp != null) {
                        mIncentivesData.postValue(resp.getIncentivi());
                    }


                } else {
                    mServiceErrorData.setValue(response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<IncentiveResponse> call, Throwable t) {
                mServiceErrorData.setValue(t.getMessage());
            }
        });
    }

    public void fetchSelectedIncentive(int id) {
        Call<Incentive> call = ItalyForMoviesClient.get().api().getSelectedIncentive(id);
        call.enqueue(new Callback<Incentive>() {
            @Override
            public void onResponse(Call<Incentive> call, Response<Incentive> response) {
                if (response.isSuccessful()) {

                    mIncentiveDetailData.postValue(response.body());

                } else {
                    mServiceErrorData.setValue(response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<Incentive> call, Throwable t) {
                mServiceErrorData.setValue(t.getMessage());
            }
        });
    }
}
