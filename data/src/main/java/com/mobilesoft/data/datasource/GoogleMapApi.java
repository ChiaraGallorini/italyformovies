package com.mobilesoft.data.datasource;

import com.mobilesoft.data.model.Predictions;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleMapApi {

    @GET("place/autocomplete/json")
    public Call<Predictions> getPlaces(
            @Query("input") String input,
            @Query("types") String types,
            @Query("language") String language,
            @Query("key") String key
    );
}
