package com.mobilesoft.data.datasource;

import androidx.lifecycle.LiveData;

import com.mobilesoft.data.model.*;

import java.util.List;

public interface RemoteDataSource {

    LiveData<List<Poi>> getPoisData();

    LiveData<PoisDetail> getPoiDetailsData();

    LiveData<List<News>> getNewsData();

    LiveData<News> getSelectedNews();


    LiveData<List<Location>> getLocationsData();

    LiveData<Location> getLocationDetailData();


    LiveData<Movie> getMovieData();

    LiveData<List<Movie>> getMovieListData();

    LiveData<List<Game>> getGameListData();

    LiveData<Game> getGameDetailData();


    LiveData<Predictions> getPredictionsData();


    LiveData<String> getError();


    LiveData<List<Incentive>> getIncentivesData();

    LiveData<Incentive> getIncentiveDetailData();
}
