package com.mobilesoft.data.datasource;

import com.mobilesoft.data.model.*;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ItalyForMoviesApi {


    //******** POI ********

    @GET("pois")
    Call<PoisResponnse> getPois(@Query("neLat") double nordEstLatitude,
                                @Query("neLng") double nordEstLongitude,
                                @Query("swLat") double sudWestLatitude,
                                @Query("swLng") double sudWestLongitude,
                                @Query("filter") String filter);

    @GET("pois/{id}")
    Call<PoisDetail> getSelectedPoi(@Path("id") int id);

    //******** POI ********


    @GET("games")
    Call<GamesResponse> getGames(@Query("page") int pageNumber);

    @GET("games/{id}")
    Call<Game> getSelectedGame(@Path("id") int id);


    //******** MOVIE ********

    @GET("films")
    Call<MovieResponse> getMovies(@Query("page") int pageNumber);

    @GET("films/{id}")
    Call<Movie> getSelectedMovie(@Path("id") int id);

    //******** NEWS ********

    @GET("news")
    Call<NewsResponse> getNews();

    @GET("news/{page}")
    Call<News> getSelectedNews(@Path("page") int pageNumber);


    //******** LOCATION ********

    @GET("locations")
    Call<LocationsResponse> getLocations(@Query("page") int pageNumber);

    @GET("locations/{id}")
    Call<Location> getSelectedLocation(@Path("id") int id);


    //******** INCENTIVE ********

    @GET("incentivi")
    Call<IncentiveResponse> getIncentives();

    @GET("incentivi/{id}")
    Call<Incentive> getSelectedIncentive(@Path("id") int id);


}
