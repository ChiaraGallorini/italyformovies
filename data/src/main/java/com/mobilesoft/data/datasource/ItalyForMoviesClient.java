package com.mobilesoft.data.datasource;

import android.util.Log;
import com.mobilesoft.data.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.Locale;

public class ItalyForMoviesClient {


    private static final String BASE_URL = "https://www.italyformovies.it/api/v1/";
    private static ItalyForMoviesClient sInstance;

    private ItalyForMoviesApi mApi;


    private ItalyForMoviesClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                String language =  Locale.getDefault().getLanguage();
                Log.d("Client", language);

                Request request = original.newBuilder()
                        .header("Accept-Language", "it")
                        .header("Content-Type", "application/json")
                        .header("Authorization", "4C4993BB2ADF37EF31394BB395BBB24A")
                        .header("Cache-Control", "max-age=86400")
                        .build();

                return chain.proceed(request);
            }
        });
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        if (BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        } else {
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }
        builder.networkInterceptors().add(interceptor);
        OkHttpClient client = builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)

                .build();

        mApi = retrofit.create(ItalyForMoviesApi.class);

    }

    static ItalyForMoviesClient get() {
        if (sInstance == null) {
            synchronized (ItalyForMoviesClient.class) {
                if (sInstance == null) {
                    sInstance = new ItalyForMoviesClient();
                }
            }
        }
        return sInstance;
    }

    ItalyForMoviesApi api() {
        return mApi;
    }
}
