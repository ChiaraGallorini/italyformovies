package com.mobilesoft.data.model

import com.google.gson.annotations.SerializedName


data class PoisResponnse(
    @SerializedName("pois")
    val pois: List<Poi>
)

data class Poi(
    @SerializedName("id")
    val id: Int,
    @SerializedName("lat")
    val latitude: Double,
    @SerializedName("lng")
    val longitude: Double,
    @SerializedName("tipologia")
    val tipology: String,
    @SerializedName("count")
    val count: Int
)

data class PoisDetail(
    @SerializedName("films")
    val movies: List<Movie>,
    @SerializedName("games")
    val games: List<Game>,
    @SerializedName("id")
    val id: Int,
    @SerializedName("image")
    val image: String,
    @SerializedName("regione")
    val region: Int,
    @SerializedName("titolo")
    val title: String
)