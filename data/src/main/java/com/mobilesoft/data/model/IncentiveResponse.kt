package com.mobilesoft.data.model

import com.google.gson.annotations.SerializedName

data class IncentiveResponse(
    @SerializedName("hasMore")
    val hasMore: Int,
    @SerializedName("incentivi")
    val incentivi: List<Incentive>,
    @SerializedName("page")
    val page: Int,
    @SerializedName("totalCount")
    val totalCount: Int
)

data class Incentive(
    @SerializedName("id")
    val id: Int,
    @SerializedName("titolo")
    val title: String,
    @SerializedName("contenuto")
    val content: String,
    @SerializedName("url_testo_fondo")
    val urlExternalLink: String
)
