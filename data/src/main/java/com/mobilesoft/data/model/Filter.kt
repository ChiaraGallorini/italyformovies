package com.mobilesoft.data.model

enum class Filter(val id: String) {
    FILM_AND_GAMES("12"),
    LOCATION("0"),
    FILM("1"),
    GAME("2"),
}