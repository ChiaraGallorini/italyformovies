package com.mobilesoft.data.model

import com.google.gson.annotations.SerializedName


data class MovieResponse(
    @SerializedName("films")
    val movies: List<Movie>,
    @SerializedName("hasMore")
    val hasMore: Int,
    @SerializedName("totalCount")
    val totalCount: Int,
    @SerializedName("page")
    val page: Int
)

data class Movie(
    @SerializedName("anno_amb")
    val yearAmb: String,
    @SerializedName("anno_prod")
    val productionYear: String,
    @SerializedName("attori")
    val actors: String,
    @SerializedName("descrizione")
    val description: String,
    @SerializedName("descrizione_video")
    val videoDescription: String,
    @SerializedName("genere")
    val genre: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("immagini")
    val images: List<Image>,
    @SerializedName("instagram")
    val instagram: String,
    @SerializedName("locandina")
    val cover: String,
    @SerializedName("locations")
    val locations: List<Location?>,
    @SerializedName("nome_produzione")
    val productionName: String,
    @SerializedName("paese")
    val country: String,
    @SerializedName("regista")
    val director: String,
    @SerializedName("titolo")
    val title: String,
    @SerializedName("url_video")
    val urlVideo: String,
    @SerializedName("anno")
    val year: String

)