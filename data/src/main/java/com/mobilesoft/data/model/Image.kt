package com.mobilesoft.data.model

import com.google.gson.annotations.SerializedName

data class Image(
        @SerializedName("desc_img")
        val name: String,
        @SerializedName("url_img")
        val urlImg: String
)