package com.mobilesoft.data.model

import com.google.gson.annotations.SerializedName

data class GamesResponse(
    @SerializedName("games")
    val games: List<Game>,
    @SerializedName("hasMore")
    val hasMore: Int,
    @SerializedName("page")
    val page: Int,
    @SerializedName("totalCount")
    val totalCount: Int
)

data class Game(
    @SerializedName("anno_prod")
    val productionYear: String,
    @SerializedName("descrizione")
    val description: String,
    @SerializedName("descrizione_video")
    val videoDescription: String,
    @SerializedName("genere")
    val genre: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("immagini")
    val images: List<Image>,
    @SerializedName("instagram")
    val instagram: String,
    @SerializedName("locandina")
    val cover: String,
    @SerializedName("locations")
    val locations: List<Location>,
    @SerializedName("sviluppo")
    val development: String,
    @SerializedName("titolo")
    val title: String,
    @SerializedName("url_video")
    val urlVideo: String,
    @SerializedName("anno")
    val year: String
)

