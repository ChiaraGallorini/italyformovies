package com.mobilesoft.data.model

import androidx.annotation.DrawableRes
import com.google.gson.annotations.SerializedName
import com.mobilesoft.data.R

data class LocationsResponse(
    @SerializedName("hasMore")
    val hasMore: Int,
    @SerializedName("locations")
    val locations: List<Location>,
    @SerializedName("page")
    val page: Int,
    @SerializedName("totalCount")
    val totalCount: Int
)

data class Location(
    @SerializedName("accesso")
    val access: String,
    @SerializedName("acqua")
    val water: Boolean,
    @SerializedName("categorie_atmosfere")
    val categoriesAtmosphere: String,
    @SerializedName("categorie_come")
    val categoriesHow: String,
    @SerializedName("categorie_dove")
    val categoriesWhere: String,
    @SerializedName("chiavi_ricerca_arc")
    val keysSearch: String,
    @SerializedName("comune")
    val common: String,
    @SerializedName("contatti")
    val contacts: List<Contact>,
    @SerializedName("descrizione_video")
    val videoDescription: String,
    @SerializedName("energia_elettrica")
    val energyElectric: Boolean,
    @SerializedName("id")
    val id: Int,
    @SerializedName("immagini")
    val images: List<Image>,
    @SerializedName("incentivi")
    val incentives: List<Incentive>,
    @SerializedName("indirizzo")
    val address: String,
    @SerializedName("instagram")
    val instagram: String,
    @SerializedName("itinerari")
    val itineraries: List<Itinerary>,
    @SerializedName("lat")
    val latitude: Double,
    @SerializedName("lng")
    val longitude: Double,
    @SerializedName("provincia")
    val district: String,
    @SerializedName("regione")
    val region: Int,
    @SerializedName("testo")
    val text: String,
    @SerializedName("tipologia")
    val tipology: String,
    @SerializedName("titolo")
    val title: String,
    @SerializedName("url_film_commission")
    val urlFilmCommission: String,
    @SerializedName("url_scheda_location")
    val urlLocationDocument: String,
    @SerializedName("url_video")
    val urlVideo: String,
    @SerializedName("url_web_loc")
    val urlWebLoc: String,
    @SerializedName("image")
    val image: String
)

data class Contact(
    @SerializedName("fax")
    val fax: String,
    @SerializedName("indirizzo")
    val address: String,
    @SerializedName("mail")
    val mail: String,
    @SerializedName("nome")
    val name: String,
    @SerializedName("telefono")
    val telephone: String
)

data class Itinerary(
    @SerializedName("id")
    val id: Int,
    @SerializedName("image")
    val image: String,
    @SerializedName("nome")
    val name: String
)



data class Position(
    val image: String,
    val title: String,
    val region: Int
)



