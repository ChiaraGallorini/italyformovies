package com.mobilesoft.data.model

import com.google.gson.annotations.SerializedName


data class NewsResponse(
    @SerializedName("news")
    val news: List<News>,
    @SerializedName("next")
    val next: String
)

data class News(
    @SerializedName("data")
    val date: Long,
    @SerializedName("id")
    val id: Int,
    @SerializedName("immagine")
    val image: String,
    @SerializedName("titolo")
    val title: String,
    @SerializedName("descrizione")
    val description: String
)