package com.mobilesoft.data.repository;

import androidx.lifecycle.LiveData;

import com.mobilesoft.data.model.*;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface Repository {

    LiveData<List<Poi>> getPoisData();

    LiveData<PoisDetail> getPoiDetail();

    LiveData<List<News>> getNewsListData();

    LiveData<String> getError();

    LiveData<List<Movie>> getMovieListData();

    LiveData<List<Game>> getGameListData();

    LiveData<Game> getSelectedGameData();

    LiveData<Movie> getSelectedMovieData();

    LiveData<News> getSelectedNewsData();

    LiveData<List<Location>> getLocationsData();

    LiveData<Location> getLocationDetailData();

    LiveData<List<Incentive>> getIncentivesData();

    LiveData<Incentive> getIncentiveDetailData();

    void fetchPoisData(double nordEstLatitude,
                       double nordEstLongitude,
                       double sudWestLatitude,
                       double sudWestLongitude,
                       String filter);

    void fetchPoiDetailData(int id);

    void fetchListNewsData();

    void fetchSelectedNewsData(int id);

    void fetchMovieListData(int page);

    void fetchGameListData(int page);

    void fetchSelectedGameData(int id);


    void fetchMovieData(int id);

    void fetchLocationsData(int page);

    void fetchLocationDetailData(int id);


    void fetchIncentitvesData();

    void fetchIncentiveDetailData(int id);
}
