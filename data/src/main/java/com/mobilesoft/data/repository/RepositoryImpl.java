package com.mobilesoft.data.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

import com.mobilesoft.data.datasource.DataSource;
import com.mobilesoft.data.model.*;

import java.util.List;

public class RepositoryImpl implements Repository {

    private final DataSource mDataSource;

    private MediatorLiveData<List<Poi>> mPoisMediator = new MediatorLiveData<>();
    private MediatorLiveData<List<News>> mNewsMediator = new MediatorLiveData<>();
    private MediatorLiveData<List<Movie>> mMoviesListMediator = new MediatorLiveData<>();
    private MediatorLiveData<List<Game>> mGamesListMediator = new MediatorLiveData<>();
    private MediatorLiveData<Game> mSelectedGameMediator = new MediatorLiveData<>();
    private MediatorLiveData<List<Location>> mLocationListMediator = new MediatorLiveData<>();
    private MediatorLiveData<Location> mLocationDetailMediator = new MediatorLiveData<>();
    private MediatorLiveData<String> mErrorMediator = new MediatorLiveData<>();
    private MediatorLiveData<News> mSelectedNewsMediator = new MediatorLiveData<>();
    private MediatorLiveData<Movie> mSelectedMovieMediator = new MediatorLiveData<>();
    private MediatorLiveData<PoisDetail> mPoiDetailMediator = new MediatorLiveData<>();
    private MediatorLiveData<Incentive> mSelectedIncentiveMediator = new MediatorLiveData<>();
    private MediatorLiveData<List<Incentive>> mIncentiveListMediator = new MediatorLiveData<>();


    private RepositoryImpl(DataSource dataSource) {
        mDataSource = dataSource;
        mPoisMediator.addSource(this.mDataSource.getPoisData(), new Observer<List<Poi>>() {
            @Override
            public void onChanged(final List<Poi> users) {

                mPoisMediator.postValue(users);
            }
        });

        mErrorMediator.addSource(this.mDataSource.getError(), new Observer<String>() {
                    @Override
                    public void onChanged(final String error) {
                        mErrorMediator.postValue(error);
                    }
                }
        );

        mSelectedMovieMediator.addSource(this.mDataSource.getMovieData(), new Observer<Movie>() {
                    @Override
                    public void onChanged(final Movie movie) {
                        mSelectedMovieMediator.postValue(movie);
                    }
                }
        );

        mNewsMediator.addSource(this.mDataSource.getNewsData(), new Observer<List<News>>() {
                    @Override
                    public void onChanged(final List<News> news) {
                        mNewsMediator.postValue(news);
                    }
                }
        );

        mMoviesListMediator.addSource(this.mDataSource.getMovieListData(), new Observer<List<Movie>>() {
                    @Override
                    public void onChanged(final List<Movie> movies) {
                        mMoviesListMediator.postValue(movies);
                    }
                }
        );

        mGamesListMediator.addSource(this.mDataSource.getGameListData(), new Observer<List<Game>>() {
                    @Override
                    public void onChanged(final List<Game> games) {
                        mGamesListMediator.postValue(games);
                    }
                }
        );

        mSelectedGameMediator.addSource(this.mDataSource.getGameDetailData(), new Observer<Game>() {
                    @Override
                    public void onChanged(final Game game) {
                        mSelectedGameMediator.postValue(game);
                    }
                }
        );
        mSelectedNewsMediator.addSource(this.mDataSource.getSelectedNews(), new Observer<News>() {
                    @Override
                    public void onChanged(final News news) {
                        mSelectedNewsMediator.postValue(news);
                    }
                }
        );

        mPoiDetailMediator.addSource(this.mDataSource.getPoiDetailsData(), new Observer<PoisDetail>() {
                    @Override
                    public void onChanged(final PoisDetail detail) {
                        mPoiDetailMediator.postValue(detail);
                    }
                }
        );

        mLocationListMediator.addSource(this.mDataSource.getLocationsData(), new Observer<List<Location>>() {
                    @Override
                    public void onChanged(final List<Location> locations) {
                        mLocationListMediator.postValue(locations);
                    }
                }
        );

        mLocationDetailMediator.addSource(this.mDataSource.getLocationDetailData(), new Observer<Location>() {
                    @Override
                    public void onChanged(final Location location) {
                        mLocationDetailMediator.postValue(location);
                    }
                }
        );

        mIncentiveListMediator.addSource(this.mDataSource.getIncentivesData(), new Observer<List<Incentive>>() {
                    @Override
                    public void onChanged(final List<Incentive> incentives) {
                        mIncentiveListMediator.postValue(incentives);
                    }
                }
        );

        mSelectedIncentiveMediator.addSource(this.mDataSource.getIncentiveDetailData(), new Observer<Incentive>() {
                    @Override
                    public void onChanged(final Incentive incentive) {
                        mSelectedIncentiveMediator.postValue(incentive);
                    }
                }
        );
    }

    public static Repository get() {
        final DataSource dataSource = new DataSource();
        return new RepositoryImpl(dataSource);
    }

    @Override
    public LiveData<List<Poi>> getPoisData() {
        return mPoisMediator;
    }

    @Override
    public LiveData<PoisDetail> getPoiDetail() {
        return mPoiDetailMediator;
    }

    @Override
    public LiveData<List<News>> getNewsListData() {
        return mNewsMediator;
    }

    @Override
    public LiveData<String> getError() {
        return mErrorMediator;
    }

    @Override
    public LiveData<List<Movie>> getMovieListData() {
        return mMoviesListMediator;
    }

    @Override
    public LiveData<List<Game>> getGameListData() {
        return mGamesListMediator;
    }

    @Override
    public LiveData<Game> getSelectedGameData() {
        return mSelectedGameMediator;
    }

    @Override
    public LiveData<Movie> getSelectedMovieData() {
        return mSelectedMovieMediator;
    }

    @Override
    public LiveData<News> getSelectedNewsData() {
        return mSelectedNewsMediator;
    }

    @Override
    public LiveData<List<Location>> getLocationsData() {
        return mLocationListMediator;
    }

    @Override
    public LiveData<Location> getLocationDetailData() {
        return mLocationDetailMediator;
    }

    @Override
    public LiveData<List<Incentive>> getIncentivesData() {
        return mIncentiveListMediator;
    }

    @Override
    public LiveData<Incentive> getIncentiveDetailData() {
        return mSelectedIncentiveMediator;
    }

    @Override
    public void fetchMovieData(int id) {
        mDataSource.fetchMovie(id);
    }

    @Override
    public void fetchPoisData(double northEastLatitude, double northEastLongitude, double southWestLatitude, double southWestLongitude, String filter) {
        mDataSource.fetchPois(northEastLatitude, northEastLongitude, southWestLatitude, southWestLongitude, filter);
    }

    @Override
    public void fetchPoiDetailData(int id) {
        mDataSource.fetchPoisDetail(id);
    }

    @Override
    public void fetchListNewsData() {
        mDataSource.fetchNews();
    }

    @Override
    public void fetchMovieListData(int page) {
        mDataSource.fetchMovies(page);
    }

    @Override
    public void fetchGameListData(int page) {
        mDataSource.fetchGames(page);
    }

    @Override
    public void fetchSelectedGameData(int id) {
        mDataSource.fetchSelectedGame(id);
    }

    @Override
    public void fetchLocationsData(int page) {
        mDataSource.fetchLocations(page);
    }

    @Override
    public void fetchSelectedNewsData(int id) {
        mDataSource.fetchSelectedNews(id);
    }

    @Override
    public void fetchLocationDetailData(int id) {
        mDataSource.fetchSelectedLocation(id);
    }

    @Override
    public void fetchIncentitvesData() {
        mDataSource.fetchIncentives();
    }

    @Override
    public void fetchIncentiveDetailData(int id) {
        mDataSource.fetchSelectedIncentive(id);
    }
}
